const $ = require('jquery');
const Dropzone = require('dropzone');
window.jQuery = $;
window.$ = $;
window.Dropzone = Dropzone;
window.Popper = require('popper.js');
require('bootstrap');
require('datatables.net')(window, $);
require('datatables.net-bs4')(window, $);
require('datatables.net-responsive-bs4')(window, $);
//hgfh
$.extend($.fn.dataTable.defaults, {
    language: {
        "sEmptyTable": "No data available in table",
        "sInfo": "Showing <em>_START_</em> to <em>_END_</em> of <em>_TOTAL_</em> entries",
        "sInfoEmpty": "",
        "sInfoFiltered": "(filtered from <em>_MAX_</em> total entries)",
        "sInfoPostFix": "",
        "sInfoThousands": ",",
        "sLengthMenu": "<span>Limit:</span> _MENU_",
        "sLoadingRecords": "Loading...",
        "sProcessing": "Processing...",
        "sSearch": "Search anywhere:",
        "sZeroRecords": "No matching records found",
        "oPaginate": {
            "sFirst": "First",
            "sLast": "Last",
            "sNext": "Next",
            "sPrevious": "Previous"
        },
        "oAria": {
            "sSortAscending": ": activate to sort column ascending",
            "sSortDescending": ": activate to sort column descending"
        }
    },

    dom: (
        "<'dt-head'<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-7'f>>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'dt-foot'<'row'<'col-sm-12 col-md-3'l><'col-sm-12 col-md-3'i><'col-sm-12 col-md-6'p>>>"
        // "<'row'<'dt-foot'<'col-sm-12 col-md-3'l><'col-sm-12 col-md-3'i><'col-sm-12 col-md-6'p>>>"
        // dt-foot
    ),

    //buttons: ['colvis'],
    // autoWidth: true,
    responsive: {
        details: {
            display: $.fn.dataTable.Responsive.display.childRowImmediate,
            type: 'column',
        }
    },
});
