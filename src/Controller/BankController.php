<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;

class BankController extends BaseController
{
    /**
     * @Route("/bank", name="bank")
     */
    public function index()
    {
        return $this->render('bank/index.html.twig', [
            'controller_name' => 'BankController',
        ]);
    }
    public function extractBankPlayers(Request $request){
        $file = $request->files->get('file');
        /** @var UploadedFile|null $file */
        // no file uploaded
        if ($file === null) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Missing file, huh?',
            ]);
        }

        // extract replay first, don't move it to public directory
        $gminfo = self::extractPlayers($file);
        return new JsonResponse($gminfo);
    }
    public function extractBank(Request $request,$id){
        $file = $request->files->get('file');
        /** @var UploadedFile|null $file */
        // no file uploaded
        if ($file === null) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Missing file, huh?',
            ]);
        }

        // extract replay first, don't move it to public directory
        $gminfo = self::extractBanks($file,$id);
        return new JsonResponse($gminfo);
    }
    private function extractPlayers(UploadedFile $uploadedFile){
        $process = new Process([
            'python2',
            $this->getParameter('kernel.project_dir').'/sc2-repdump/s2repdump/main.py',
            $uploadedFile->getPathname(),
            "--players"
        ]);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        $output=$process->getOutput();
        $output=strstr($output,"{");
        return json_decode($output, true);
    }
    private function extractBanks(UploadedFile $uploadedFile,$id){
        //create banks
        $time=time();
        $pathOUT=$this->getParameter('kernel.project_dir') . "/public/uploads/banks";
        $path=$pathOUT."/".time();
        $proc="s2repdump --bank ".$id." --out ".$path."/ ".$uploadedFile->getPathname();
        //$process = new Process($proc);
        $process = new Process([
            'python2',
            $this->getParameter('kernel.project_dir').'/sc2-repdump/s2repdump/main.py',
            "--bank",
            $id,
            "--out",
            $this->getParameter('kernel.project_dir') . "/public/uploads/banks/$time/",
            $uploadedFile->getPathname(),
        ]);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        //zip banks
        $processZip = new Process("cd '$pathOUT' && zip -r '$time.zip' '$time' && rm -rf '$time'");
        $processZip->run();
        if (!$processZip->isSuccessful()) {
            throw new ProcessFailedException($processZip);
        }
        return "$time.zip";
    }
}
