<?php

namespace App\Controller;

use Carbon\Carbon;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\BaseController;

class AjaxController extends BaseController
{
    public function findEscapes(Connection $pdo, Request $request)
    {
        return $this->json(['data' => $this->getEscapesData()]);
    }

    public function getEscapesData()
    {

        $currentCount = (int) $this->conn->query('SELECT COUNT(*) FROM RUN')->fetchColumn();
        $escapesData = $this->cache->getItem('escapes_data');
        if ($escapesData->isHit() && $currentCount === count($escapesData->get())) {
            //return $escapesData->get();
        }
        
        $reqsql = "SELECT
        `run`.`RUN_ID` AS `RUN_ID`,
        `run`.`REVIVES` AS `REVIVES`,
        `run`.`DEATHS` AS `DEATHS`,
        `run`.`BONUS` AS `BONUS`,
        `run`.`STIM` AS `STIM`,
        `run`.`CREEP` AS `CREEP`,
        `run`.`RING` AS `RING`,
        `run`.`ART` AS `ART`,
        `run`.`REVART` AS `REVART`,
        `run`.`TIMESHIFT` AS `TIMESHIFT`,
        `run`.`LEVELUPS` AS `LEVELUPS`,
        `run`.`VERSION` AS `VERSION`,
        `run`.`ELAPSED_REAL_TIME` AS `ELAPSED_REAL_TIME`,
        `reg`.`NAME_SHORT` AS `REGION`,
        `run`.`TIME` AS `TIME`,
        `run`.`SUBMITED_ON` AS `SUBMITED_ON`,
        `GAME`.`NAME_SHORT` AS `NAME`,
        `DIFFICULTY`.`NAME` AS `DIF_NAME`,
        `SPEED`.`NAME` AS `SPEED_NAME`,
        (SELECT
                GROUP_CONCAT(CONCAT(`pp`.`NAME`,
                                '/',
                                 `crcr`.`COLOR`,'/',crcr.LEFT )
                        ORDER BY `pp`.`NAME` ASC
                        SEPARATOR '|')
            FROM
                ((`PLAYER` `pp`
                JOIN `CHARACTER` `cc` ON ((`pp`.`PLAYER_ID` = `cc`.`PLAYER_ID`)))
                JOIN `CHARACTER_RUN` `crcr` ON ((`crcr`.`CHARACTER_ID` = `cc`.`CHARACTER_ID`)))
            WHERE
                ((`crcr`.`RUN_ID` = `run`.`RUN_ID`)
                    AND (`pp`.`TYPE` = 'USER'))) AS `PLAYERS`,
        IF(((SELECT
                    `rr`.`RUN_ID`
                FROM
                    (`RUN` `rr`
                    JOIN `GAMEMODE` `gmgm` ON ((`rr`.`GAMEMODE_ID` = `gmgm`.`GAMEMODE_ID`)))
                WHERE
                    ((`gmgm`.`GAME_ID` = `gm`.`GAME_ID`)
                        AND (`gmgm`.`DIFFICULTY_ID` = `gm`.`DIFFICULTY_ID`))
                ORDER BY `rr`.`TIME`
                LIMIT 1) = `run`.`RUN_ID`),
            1,
            0) AS `WR`,
        IF(ISNULL(`v`.`URL`), '0', `v`.`URL`) AS `VIDEO_URL`
    FROM
        ((((((`RUN` `run`
        JOIN `GAMEMODE` `gm` ON ((`run`.`GAMEMODE_ID` = `gm`.`GAMEMODE_ID`)))
        JOIN `REGION` `reg` ON ((`run`.`REGION_ID` = `reg`.`REGION_ID`)))
        JOIN `GAME` ON ((`gm`.`GAME_ID` = `GAME`.`GAME_ID`)))
        JOIN `DIFFICULTY` ON ((`DIFFICULTY`.`DIFFICULTY_ID` = `gm`.`DIFFICULTY_ID`)))
        JOIN `SPEED` ON ((`SPEED`.`SPEED_ID` = `gm`.`SPEED_ID`)))
        LEFT JOIN `VIDEO` `v` ON ((`v`.`RUN_ID` = `run`.`RUN_ID`)))
    ORDER BY `run`.`RUN_ID`";

        $req = $this->conn->prepare($reqsql);
        $req->execute();
        $result = [
            'data' => [],
        ];
        while ($data = $req->fetch()) {
            $id = $data["RUN_ID"];
            $players_data = $data["PLAYERS"];
            $players = explode("|",$players_data);
            $players_str = "";
            foreach ($players as $player) {
                $player_data =explode("/",$player);
                $name = $player_data[0];
                $color = $player_data[1];
                $ppl_str = "<span style='color:rgb($color);' class='player-name'>";
                if ($player_data[2] == 1) {
                    $ppl_str .= "<strike>";
                }
                $ppl_str .= $name;
                if ($player_data[2] == 1) {
                    $ppl_str .= "</strike>";
                }
                $ppl_str .= "</span> ";
                $players_str .= $ppl_str;
            }
            $time = $data["TIME"];
            $name = $data["NAME"];
            $diff_name = $data["DIF_NAME"];
            $speed_name = $data["SPEED_NAME"];
            $region = $data["REGION"];
            $wr = $data["WR"];

            $seconds = $time;
            $hours = floor($seconds / 3600);
            $minutes = floor(($seconds / 60) % 60);
            $seconds = $seconds % 60;
            $ms = $time - floor($time);

            $formattedTime = [];
            if ($hours > 0) {
                $formattedTime[] = "{$hours}h";
            }

            if ($minutes >= 0) {
                $formattedTime[] = str_pad($minutes, 2, '0', STR_PAD_LEFT) . "<small>m</small>";
            }

            if ($seconds >= 0) {
                $formattedTime[] = str_pad($seconds, 2, '0', STR_PAD_LEFT) . "<small>s</small>";
                // . ($hours <= 0 ? "<small>," . (round($ms, 1) * 10) . "</small>" : "") . "<small>s</small>";
            }

            $formattedTime = implode('&nbsp;', $formattedTime);

            $link = $this->generateUrl('run_details', ['run_id' => $id]);

            $colValues = [];
            $colValues['players'] = [
                'formatted' => $players_str,
                'raw' => count($players),
            ]; 
            $colValues['time'] = [
                'formatted' => $formattedTime,
                'raw' => floor($data["TIME"] * 100.0),
            ]; 
            $colValues['game'] = $name;
            $colValues['difficulty'] = $diff_name;
            $colValues['speed'] = $speed_name;
            $colValues['region'] = $region;
            $colValues['submitted_on'] =[
                'formatted' => Carbon::createFromTimestamp(strtotime($data['SUBMITED_ON']))->diffForHumans(Carbon::now(), \Carbon\CarbonInterface::DIFF_RELATIVE_TO_NOW),
                'raw' =>strtotime($data['SUBMITED_ON'])

            ] ;
            $colValues['action'] = "<a class=\"text-success\" href=\"/escape/$id\"><i class=\"fas fa-search-plus\"></i></a><span class=\"sr-only\">See more</span>";
            $colValues['wr'] = (int) $wr;
            $colValues['id'] = (int) $id;
            $colValues['recorded'] = (int) ($data['VIDEO_URL'] !== '0');
            $result['data'][] = $colValues;
        }

        $escapesData->set($result['data']);
        $escapesData->expiresAfter(3600 * 24 * 7);
        $this->cache->save($escapesData);

        return $result['data'];
    }
    public function findPlayers(Connection $pdo, Request $request)
    {
        $reqsql = "SELECT (SELECT RUN_ID FROM RUN as rr JOIN GAMEMODE as gmgm on rr.GAMEMODE_ID=gmgm.GAMEMODE_ID WHERE rr.TIME=MIN(run.TIME) AND gmgm.DIFFICULTY_ID=gm.DIFFICULTY_ID AND gmgm.GAME_ID=gm.GAME_ID ORDER BY rr.TIMESTAMP LIMIT 1 ) AS RUN_ID,
        MIN(run.TIME) AS TIME,
        GAME.NAME_SHORT,
        DIFFICULTY.NAME AS 'DIF_NAME',
       (SELECT ss.NAME FROM RUN as rr JOIN GAMEMODE as gmgm on rr.GAMEMODE_ID=gmgm.GAMEMODE_ID JOIN SPEED as ss on gmgm.SPEED_ID=ss.SPEED_ID WHERE rr.TIME=MIN(run.TIME) AND gmgm.DIFFICULTY_ID=gm.DIFFICULTY_ID AND gmgm.GAME_ID=gm.GAME_ID ORDER BY rr.TIMESTAMP LIMIT 1 ) AS 'SPEED_NAME',
        gm.GAME_ID,
        (SELECT
            GROUP_CONCAT(pp.NAME SEPARATOR ',')
            FROM
                PLAYER as pp
                JOIN `CHARACTER` as cc ON pp.PLAYER_ID = cc.PLAYER_ID
                JOIN `CHARACTER_RUN` as crcr ON crcr.CHARACTER_ID =cc.CHARACTER_ID
                WHERE crcr.RUN_ID=(SELECT RUN_ID FROM RUN as rr JOIN GAMEMODE as gmgm on rr.GAMEMODE_ID=gmgm.GAMEMODE_ID WHERE rr.TIME=MIN(run.TIME) AND gmgm.DIFFICULTY_ID=gm.DIFFICULTY_ID AND gmgm.GAME_ID=gm.GAME_ID ORDER BY rr.TIMESTAMP LIMIT 1 ) AND pp.TYPE='USER' AND crcr.left=0
                ) as PLAYERS
            FROM RUN as run
            JOIN GAMEMODE as gm ON run.GAMEMODE_ID = gm.GAMEMODE_ID
            JOIN GAME ON gm.GAME_ID=GAME.GAME_ID
            JOIN DIFFICULTY ON DIFFICULTY.DIFFICULTY_ID=gm.DIFFICULTY_ID
            JOIN SPEED ON SPEED.SPEED_ID=gm.SPEED_ID
            GROUP BY gm.GAME_ID,gm.DIFFICULTY_ID";

        $req = $pdo->prepare($reqsql);
        $req->execute();
        $players = [];
        $players_wrs = [];
        while ($data = $req->fetch()) {
            $ppls = $data["PLAYERS"];
            $game = "<a href='/escape/" . $data["RUN_ID"] . "'>" . $data["NAME_SHORT"] . " " . $data["DIF_NAME"] . " " . $data["SPEED_NAME"] . "</a>";
            $ppls = explode(",", $ppls);
            foreach ($ppls as $ppl) {
                $player = null;
                if (array_key_exists($ppl, $players)) {
                    $player = $players[$ppl];
                }

                if (is_array($player) === false) {
                    $player = [];
                }
                array_push($player, $game);

                if (array_key_exists($ppl, $players_wrs)) {
                    $player_wrs = $players[$ppl];
                } else {
                    $players_wrs[$ppl] = 0;
                }
                $players_wrs[$ppl] = $players_wrs[$ppl] + 1;
                $players[$ppl] = $player;
            }
        }
        asort($players_wrs);
        foreach ($players_wrs as $ppl_wrs => $wr_nb) {
            foreach ($players as $ppl => $wrs) {
                if ($ppl_wrs === $ppl) {
                    $players = array_merge([$ppl => $players[$ppl]], $players);
                }
            }
        }

        $first = 1;
        $i = 1;
        $place = 1;
        $old_wr_nb = 1;
        foreach ($players as $ppl => $wr_nb) {
            if ($first == 1) {
                $table = "{\"data\":[";
                $first = 0;
            }
            if (count($wr_nb) != $old_wr_nb) {
                $place = $i;
            }

            $table .= "[\"";
            $table .= $place;
            $table .= "\",\"";
            $table .= $ppl;
            $table .= "\",\"";
            $table .= count($wr_nb);
            $table .= "\",\"";
            $table .= implode(" / ", $wr_nb);
            $table .= "\"],";
            $i++;
            $old_wr_nb = count($wr_nb);
        }
        if ($first == 0) {
            $table = substr($table, 0, -1);
            $table .= "]}";
        } else {
            $table = "{ \"data\": [] }";
        }

        return JsonResponse::fromJsonString($table);
    }
    private function getPlayers($pdo, $run_id, $type)
    {
        $reqsql_players = "SELECT *,p.PLAYER_ID AS PPL FROM PLAYER as p
            JOIN `CHARACTER` as c ON p.PLAYER_ID=c.PLAYER_ID
            JOIN CHARACTER_RUN as cr ON c.CHARACTER_ID=cr.CHARACTER_ID WHERE RUN_ID=? AND p.TYPE='$type'";
        $req_players = $pdo->prepare($reqsql_players);
        $req_players->bindParam(1, $run_id);
        $req_players->execute();
        $data = $req_players->fetchAll();
        return $data;
    }
}
