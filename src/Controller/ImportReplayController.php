<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ImportReplayController extends AbstractController
{
    /**
     * @Route("/import/replay", name="import_replay")
     */
    public function index()
    {
        return $this->render('import_replay/index.html.twig', [
            'controller_name' => 'ImportReplayController',
        ]);
    }
}
