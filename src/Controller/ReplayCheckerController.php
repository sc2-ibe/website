<?php

namespace App\Controller;

use Doctrine\DBAL\Driver\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Routing\Annotation\Route;

class ReplayCheckerController extends AbstractController
{
    /**
     * @var Connection
     */
    protected $conn;

    /**
     * @var ExtractReplay
    */
    protected $extractReplay;

    public function __construct(Connection $conn, ExtractReplay $extractReplay)
    {
        $this->conn = $conn;
        $this->extractReplay = $extractReplay;
    }

    /**
     * @Route("/replay-analysis", name="replay_checker")
     */
    public function index(Request $request)
    {
        $file = $request->files->get('file');
        /** @var UploadedFile|null $file */

        // no file uploaded
        if ($file === null) {
            return $this->render('replay_checker/index.html.twig');
        }

        try {
            $gminfo = $this->extractReplay->extract($file->getPathname(), ['--include-loss']);
        } catch (ProcessFailedException $e) {
            switch ($e->getProcess()->getExitCode()) {
                // not supported
                case 2:
                {
                    $this->addFlash('error', 'Replay file damaged/corrupted. Likely played on too old SC2 version.');
                    return $this->render('replay_checker/index.html.twig');
                    break;
                }

                // internal error
                default:
                {
                    throw $e;
                    break;
                }
            }
        }

        $gameMode = $this->extractReplay->getGameMode($gminfo['map'], $gminfo['results'][0]);
        if ($gameMode === false) {
            $this->addFlash('error', 'Unknown game mode / not supported map.');
            return $this->render('replay_checker/index.html.twig');
        }
        
        $bestTimes = $this->getBestTimes($gminfo["map"]["id"]);
        $runs = $this->getChallenges($gminfo, $bestTimes);

        return $this->render('replay_checker/index.html.twig', [
            'runs' => $runs,
        ]);
    }

    private function getChallenges($gminfo, $bestTimes)
    {
        $runs = [];
        foreach ($gminfo["results"] as $resRun) {
            $challenges = [];
            $totalTime = 0;
            $totalBestTime = 0;
            foreach ($resRun["challenges"] as $key => $resChal) {
                $dataChallenge = $this->getBestTime($bestTimes, $key, $resRun["game_diff"]);
                $challenge = [];
                $challenge["name"] = $dataChallenge["name"];
                $challenge["bestTime"] = $dataChallenge["time"];
                $challenge["time"] = $resChal["completed_time"];
                if ($resChal['completed_by'] !== null) {
                    $totalTime += $resChal["completed_time"];
                    $totalBestTime += $dataChallenge["time"];
                }
                $challenges[] = $challenge;
            }

            $runs[] = [
                'challenges' => $challenges,
                'timeDiff' => $totalTime - $totalBestTime,
            ];
        }
        return $runs;
    }

    private function getBestTime($bestTimes, $number, $difficulty)
    {
        $data = [];
        foreach ($bestTimes as $bestTime) {
            if ($bestTime["number"] == $number && $bestTime["diff"] == $difficulty) {
                $data["time"] = $bestTime["time"];
                $data["name"] = 'L' . str_pad($bestTime["number"], 2, '0', STR_PAD_LEFT) . " " . $bestTime["name"];
            }
        }

        return $data;
    }

    private function getBestTimes($game_code)
    {
        $reqsql_challenges = "SELECT MIN(rm.TIME) AS BEST_TIME,gm.DIFFICULTY_ID,m.MAP_NUMBER,MAP_NAME
            FROM RUN_MAP as rm
            JOIN RUN as r ON r.RUN_ID=rm.RUN_ID
            JOIN MAP as m ON m.MAP_ID=rm.MAP_ID
            JOIN GAMEMODE as gm ON gm.GAMEMODE_ID=r.GAMEMODE_ID
            JOIN GAME as g ON g.GAME_ID=gm.GAME_ID
            WHERE g.CODE_NAME=? AND gm.DIFFICULTY_ID=1
            AND NOT (r.BUILD>=38215 AND r.VERSION<55 AND rm.MAP_ID=172) AND NOT (r.VERSION<9 AND rm.MAP_ID IN (189,190))
            GROUP BY rm.MAP_ID,gm.DIFFICULTY_ID
            UNION
            SELECT MIN(rm.TIME) AS BEST_TIME,gm.DIFFICULTY_ID,m.MAP_NUMBER,MAP_NAME
            FROM RUN_MAP as rm
            JOIN RUN as r ON r.RUN_ID=rm.RUN_ID
            JOIN MAP as m ON m.MAP_ID=rm.MAP_ID
            JOIN GAMEMODE as gm ON gm.GAMEMODE_ID=r.GAMEMODE_ID
            JOIN GAME as g ON g.GAME_ID=gm.GAME_ID
            WHERE g.CODE_NAME=? AND gm.DIFFICULTY_ID=2
            AND NOT (r.BUILD>=38215 AND r.VERSION<55 AND rm.MAP_ID=172) AND NOT (r.VERSION<9 AND rm.MAP_ID IN (189,190))
            GROUP BY rm.MAP_ID,gm.DIFFICULTY_ID";
        $req_challenges = $this->conn->prepare($reqsql_challenges);
        $req_challenges->bindParam(1, $game_code);
        $req_challenges->bindParam(2, $game_code);
        $req_challenges->execute();
        $bestTimes = [];
        while ($data_challenges = $req_challenges->fetch()) {
            $bestTime = [];
            $bestTime["number"] = $data_challenges["MAP_NUMBER"];
            $bestTime["time"] = $data_challenges["BEST_TIME"];
            $bestTime["diff"] = $data_challenges["DIFFICULTY_ID"];
            $bestTime["name"] = $data_challenges["MAP_NAME"];
            array_push($bestTimes, $bestTime);
        }
        return $bestTimes;
    }
}
