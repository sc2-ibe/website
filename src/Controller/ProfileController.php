<?php

namespace App\Controller;

use Doctrine\DBAL\Driver\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{
    /**
     * @var Connection
     */
    protected $conn;
    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }
    /**
     *  @Route("/profile/{ppl_id}", name="profile", requirements={"ppl_id"="\d+"})
     */
    public function index($ppl_id)
    {
        $wrs = $this->getWrCount($ppl_id);
        $challenges = $this->getChallenges($ppl_id);
        $powerups = $this->getPowerUps($ppl_id);
        $buttons = $this->getButtons($ppl_id);
        $timeSpent = $this->getTimeSpent($ppl_id);
        $mostEscapedGame = $this->getMostEscapedGame($ppl_id);
        $lastEscape = $this->getLastEscape($ppl_id);
        $name = $this->getName($ppl_id);
        return $this->render('profile/index.html.twig', [
            'wr_count' => $wrs,
            'challenges' => $challenges,
            'powerups' => $powerups,
            'buttons' => $buttons,
            'timeSpent' => $timeSpent,
            'mostEscapedGame' => $mostEscapedGame,
            'lastEscape' => $lastEscape,
            'ppl_id' => $ppl_id,
            'name' => $name,
        ]);
    }
    private function getWrCount($ppl_id)
    {
        $reqsql = "SELECT
            IF(MIN(TIME)=(SELECT MIN(TIME) FROM RUN as rr JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID JOIN `CHARACTER` as cc on crcr.CHARACTER_ID=cc.CHARACTER_ID JOIN GAMEMODE as gmgm on rr.GAMEMODE_ID=gmgm.GAMEMODE_ID WHERE gmgm.GAME_ID=gm.GAME_ID AND gmgm.DIFFICULTY_ID=gm.DIFFICULTY_ID GROUP BY gmgm.GAME_ID,DIFFICULTY_ID),1,0) AS ISWR
            FROM RUN as r
            JOIN CHARACTER_RUN as cr on r.RUN_ID=cr.RUN_ID
            JOIN `CHARACTER` as c on cr.CHARACTER_ID=c.CHARACTER_ID
            JOIN GAMEMODE as gm on r.GAMEMODE_ID=gm.GAMEMODE_ID
            WHERE PLAYER_ID=?
            GROUP BY gm.GAME_ID,DIFFICULTY_ID";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $ppl_id);
        $req->execute();
        $wr_count = 0;
        while ($data = $req->fetch()) {
            if ($data["ISWR"] == "1") {
                $wr_count = $wr_count + 1;
            }
        }
        return $wr_count;
    }
    private function getName($ppl_id)
    {
        $reqsql = "SELECT
                NAME 
            FROM PLAYER 
            WHERE PLAYER_ID=?";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $ppl_id);
        $req->execute();
        $data = $req->fetch();
        return $data["NAME"];
    }
    private function getChallenges($ppl_id)
    {
        $reqsql = "SELECT
                count(*) AS MAP_COUNT
            FROM RUN_MAP as rm
            JOIN RUN_MAP_COMPLETE as rmc on rm.RUN_MAP_ID=rmc.RUN_MAP_ID
            JOIN `CHARACTER` as c on rmc.CONTROLLED_BY=c.CHARACTER_ID
            WHERE PLAYER_ID=?";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $ppl_id);
        $req->execute();
        $data = $req->fetch();
        return $data["MAP_COUNT"];
    }
    private function getPowerUps($ppl_id)
    {
        $reqsql = "SELECT
        count(*) as POWERUP_COUNT
        FROM RUN_MAP_POWERUP as p
		JOIN RUN_MAP_COMPLETE as rmc on p.RUN_MAP_ID=rmc.RUN_MAP_ID
		JOIN `CHARACTER` as c on rmc.CONTROLLED_BY=c.CHARACTER_ID
         WHERE PLAYER_ID=?";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $ppl_id);
        $req->execute();
        $data = $req->fetch();
        return $data["POWERUP_COUNT"];
    }
    private function getButtons($ppl_id)
    {
        $reqsql = "SELECT
        count(*) as BUTTON_COUNT
        FROM RUN_MAP_BUTTON as b
		JOIN RUN_MAP_COMPLETE as rmc on b.RUN_MAP_ID=rmc.RUN_MAP_ID
		JOIN `CHARACTER` as c on rmc.CONTROLLED_BY=c.CHARACTER_ID
         WHERE PLAYER_ID=?";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $ppl_id);
        $req->execute();
        $data = $req->fetch();
        return $data["BUTTON_COUNT"];
    }
    private function getTimeSpent($ppl_id)
    {
        $reqsql = "SELECT
    SUM(ELAPSED_REAL_TIME) as ELAPSED_TIME
        FROM
            RUN AS r
                JOIN
            CHARACTER_RUN AS cr ON r.RUN_ID = cr.RUN_ID
                JOIN
            `CHARACTER` AS c ON cr.CHARACTER_ID = c.CHARACTER_ID
            WHERE PLAYER_ID=?";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $ppl_id);
        $req->execute();
        $data = $req->fetch();
        return $data["ELAPSED_TIME"];
    }
    private function getMostEscapedGame($ppl_id)
    {
        $reqsql = "SELECT
            g.NAME AS NAME
                FROM
                    RUN AS r
                        JOIN
                    CHARACTER_RUN AS cr ON r.RUN_ID = cr.RUN_ID
                        JOIN
					`CHARACTER` AS c ON cr.CHARACTER_ID = c.CHARACTER_ID
                    JOIN GAMEMODE as gm on r.GAMEMODE_ID=gm.GAMEMODE_ID
                    JOIN GAME as g on gm.GAME_ID=g.GAME_ID
                    WHERE PLAYER_ID=?
                    GROUP BY gm.GAME_ID
                    ORDER BY(count(*)) DESC
            LIMIT 1";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $ppl_id);
        $req->execute();
        $data = $req->fetch();
        return $data["NAME"];
    }
    private function getLastEscape($ppl_id)
    {
        $reqsql = "SELECT
    TIMESTAMP
FROM
    RUN AS r
       JOIN
        CHARACTER_RUN AS cr ON r.RUN_ID = cr.RUN_ID
            JOIN
        `CHARACTER` AS c ON cr.CHARACTER_ID = c.CHARACTER_ID
        JOIN GAMEMODE as gm on r.GAMEMODE_ID=gm.GAMEMODE_ID
        JOIN GAME as g on gm.GAME_ID=g.GAME_ID
    WHERE PLAYER_ID=?
    ORDER BY timestamp DESC LIMIT 1";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $ppl_id);
        $req->execute();
        $data = $req->fetch();
        return $data["TIMESTAMP"];
    }

    //challgengestime ajax call
    public function getProfileTimes($id)
    {
        $reqsql = "SELECT *,
		(SELECT GROUP_CONCAT(CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=1 AND gmgm.SPEED_ID=1 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190))AND cc.PLAYER_ID=?  ) AS TIMES_NORMAL_NORMAL ,
		(SELECT GROUP_CONCAT(CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=1 AND gmgm.SPEED_ID=2 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190)) AND cc.PLAYER_ID=?) AS TIMES_NORMAL_FAST,
		(SELECT GROUP_CONCAT(CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=1 AND gmgm.SPEED_ID=3 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190)) AND cc.PLAYER_ID=?) AS TIMES_NORMAL_SPEEDY,
		(SELECT GROUP_CONCAT(CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=2 AND gmgm.SPEED_ID=1 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190)) AND cc.PLAYER_ID=?) AS TIMES_EXT_NORMAL ,
		(SELECT GROUP_CONCAT(CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=2 AND gmgm.SPEED_ID=2 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190)) AND cc.PLAYER_ID=?) AS TIMES_EXT_FAST,
		(SELECT GROUP_CONCAT(CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=2 AND gmgm.SPEED_ID=3 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190)) AND cc.PLAYER_ID=?) AS TIMES_EXT_SPEEDY
         ,CLASS
         FROM MAP as m JOIN GAME as g ON m.GAME_ID=g.GAME_ID";
        $first = 1;
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $id);
        $req->bindParam(2, $id);
        $req->bindParam(3, $id);
        $req->bindParam(4, $id);
        $req->bindParam(5, $id);
        $req->bindParam(6, $id);
        $req->execute();
        $result = [
            'data' => [],
        ];
        while ($data = $req->fetch()) {
            $colValues = [];

            $colValues['game'] = $data["NAME_SHORT"];
            $class = $data["CLASS"];
            if ($data["GAME_ID"] == 1) {
                $img_name = "1_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 2) {
                $img_name = "2_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 3) {
                $img_name = "21_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 4) {
                $img_name = "cv_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 5) {
                $img_name = "cv-ez_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 6) {
                $img_name = "cv-pro_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 11) {
                $img_name = "1_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 12) {
                $img_name = "btb_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 13) {
                $img_name = "btb-pro_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 14) {
                $img_name = "btb-bane_" . $data["MAP_NUMBER"] . ".jpg";
            }

            $timesNN = $data["TIMES_NORMAL_NORMAL"];
            if ($timesNN != null) {
                $timesNN = explode('<sep3>', $timesNN);
                $timesNN = array_slice($timesNN, 0, 10, true);
            } else {
                $timesNN = [];
            }
            $timesEN = $data["TIMES_EXT_NORMAL"];
            if ($timesEN != null) {
                $timesEN = explode('<sep3>', $timesEN);
                $timesEN = array_slice($timesEN, 0, 10, true);
            } else {
                $timesEN = [];
            }
            $timesNF = $data["TIMES_NORMAL_FAST"];
            if ($timesNF != null) {
                $timesNF = explode('<sep3>', $timesNF);
                $timesNF = array_slice($timesNF, 0, 10, true);
            } else {
                $timesNF = [];
            }
            $timesEF = $data["TIMES_EXT_FAST"];
            if ($timesEF != null) {
                $timesEF = explode('<sep3>', $timesEF);
                $timesEF = array_slice($timesEF, 0, 10, true);
            } else {
                $timesEF = [];
            }
            $timesNS = $data["TIMES_NORMAL_SPEEDY"];
            if ($timesNS != null) {
                $timesNS = explode('<sep3>', $timesNS);
                $timesNS = array_slice($timesNS, 0, 10, true);
            } else {
                $timesNS = [];
            }
            $timesES = $data["TIMES_EXT_SPEEDY"];
            if ($timesES != null) {
                $timesES = explode('<sep3>', $timesES);
                $timesES = array_slice($timesES, 0, 10, true);
            } else {
                $timesES = [];
            }
            $finished = "UNFINISHED";
            $bestTime = "";
            $bestTimeNN = null;
            $bestTimeNF = null;
            $bestTimeNS = null;
            $bestTimeEN = null;
            $bestTimeEF = null;
            $bestTimeES = null;
            if (count($timesNN) > 0) {
                //$bestTime .= "<span class='span-best-times'>Normal Normal: <br/>";
                $bestTime .= "<div class='span-best-times'>Normal Normal: <br/>";
                $index = 1;
                $bestTimeNN = explode('<sep>', $timesNN[0])[0];
                foreach ($timesNN as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }
                $bestTime .= "</div>";
                $finished = "FINISHED";
            }
            if (count($timesNF) > 0) {
                $bestTime .= "<div class='span-best-times'>Normal Fast: <br/>";
                $index = 1;

                $bestTimeNF = explode('<sep>', $timesNF[0])[0];
                foreach ($timesNF as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }
                $bestTime .= "</div>";
                $finished = "FINISHED";
            }
            if (count($timesNS) > 0) {
                $bestTime .= "<div class='span-best-times'>Normal Speedy: <br/>";
                $index = 1;

                $bestTimeNS = explode('<sep>', $timesNS[0])[0];
                foreach ($timesNS as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }
                $bestTime .= "</div>";
                $finished = "FINISHED";
            }
            if (count($timesEN) > 0) {
                $bestTime .= "<div class='span-best-times'> Extreme Normal: <br/>";
                $index = 1;

                $bestTimeEN = explode('<sep>', $timesEN[0])[0];
                foreach ($timesEN as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }
                $bestTime .= "</div>";
                $finished = "FINISHED";
            }
            if (count($timesEF) > 0) {
                $bestTime .= "<div class='span-best-times'>Extreme fast: <br/>";
                $index = 1;

                $bestTimeEF = explode('<sep>', $timesEF[0])[0];
                foreach ($timesEF as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }
                $bestTime .= "</div>";
                $finished = "FINISHED";
            }
            if (count($timesES) > 0) {
                $bestTime .= "<div class='span-best-times'>Extreme Speedy:<br/>";
                $index = 1;

                $bestTimeES = explode('<sep>', $timesES[0])[0];
                foreach ($timesES as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }
                $bestTime .= "</div>";
                $finished = "FINISHED";
            }
            $img = "<img alt='" . $data["MAP_NAME"] . "'class='img-fluid challenge-img' onclick='enlargeImg(this.src)' src='/uploads/img/maps/thumb/" . $img_name . "' />";
            $colValues['challenge'] = $data["MAP_NUMBER"] . ". " . $data["MAP_NAME"];
            $colValues['bestTime'] = htmlspecialchars_decode($bestTime);
            if ($class == 1) {
                if ($bestTimeNN !== null) {
                    $colValues['bestTimeNN'] = $bestTimeNN;
                } else {
                    $colValues['bestTimeNN'] = -1000000000;
                }
                if ($bestTimeNF !== null) {
                    $colValues['bestTimeNF'] = $bestTimeNF;
                } else {
                    $colValues['bestTimeNF'] = -1000000000;
                }
                if ($bestTimeNS != null) {
                    $colValues['bestTimeNS'] = $bestTimeNS;
                } else {
                    $colValues['bestTimeNS'] = -1000000000;
                }
                if ($bestTimeEN != null) {
                    $colValues['bestTimeEN'] = $bestTimeEN;
                } else {
                    $colValues['bestTimeEN'] = -11000000000;
                }
                if ($bestTimeEF != null) {
                    $colValues['bestTimeEF'] = $bestTimeEF;
                } else {
                    $colValues['bestTimeEF'] = -11000000000;
                }
                if ($bestTimeES != null) {
                    $colValues['bestTimeES'] = $bestTimeES;
                } else {
                    $colValues['bestTimeES'] = -11000000000;
                }
            } else {
                $colValues['bestTimeNN'] = 0;
                $colValues['bestTimeNF'] = 0;
                $colValues['bestTimeNS'] = 0;
                $colValues['bestTimeEN'] = 0;
                $colValues['bestTimeEF'] = 0;
                $colValues['bestTimeES'] = 0;
            }

            $colValues['state'] = $finished;
            $colValues['class'] = $data["CLASS"];
            $colValues['img'] = $img;

            $result['data'][] = $colValues;
        }
        return new JsonResponse($result);
    }
    private function secs_to_str($duration)
    {
        $seconds = $duration;
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);
        $seconds = $seconds % 60;
        $ms = $duration - floor($duration);

        $formattedTime = [];
        if ($hours > 0) {
            $formattedTime[] = "{$hours}:";
        }

        if ($minutes >= 0) {
            $formattedTime[] = str_pad($minutes, 2, '0', STR_PAD_LEFT) . ":";
        }

        if ($seconds >= 0) {
            $formattedTime[] = str_pad($seconds, 2, '0', STR_PAD_LEFT) . ($hours <= 0 ? "'<small>" . str_pad((round($ms, 2) * 100), 2, '0', STR_PAD_LEFT) . "</small>" : "") . "";
            // . ($hours <= 0 ? "<small>," . (round($ms, 1) * 10) . "</small>" : "") . "<small>s</small>";
        }

        $formattedTime = implode('', $formattedTime);
        return $formattedTime;
    }
}
