<?php

namespace App\Controller;

use Doctrine\DBAL\Driver\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ExtractTimesController extends AbstractController
{
    protected $instance_nb;
    protected $CUser;
    /**
     * @Route("/extract/times", name="extract_times")
     */
    public function index(Connection $pdo)
    {
        $this->instance_nb = 1;
        $root = new \SimpleXMLElement("<?xml version='1.0' ?><Catalog></Catalog>");
        $this->CUser = $root->addChild('CUser');
        $this->CUser->addAttribute('Id', 'ESCAPES');
        $this->CUser->addAttribute('id', 'ESCAPES');

        $fieldTime = $this->CUser->addChild("Fields");
        $fieldTime->addAttribute('Id', 'TIME');
        $fieldTime->addAttribute('Type', 'Fixed');

        $fieldPlayers = $this->CUser->addChild("Fields");
        $fieldPlayers->addAttribute('Id', 'PLAYERS');
        $fieldPlayers->addAttribute('Type', 'String');
        $fieldPlayers->addAttribute('Count', '12');

        $fieldGame = $this->CUser->addChild("Fields");
        $fieldGame->addAttribute('Id', 'GAME');
        $fieldGame->addAttribute('Type', 'String');

        $fieldDiff = $this->CUser->addChild("Fields");
        $fieldDiff->addAttribute('Id', 'DIFFICULTY');
        $fieldDiff->addAttribute('Type', 'Int');

        $fieldDiff = $this->CUser->addChild("Fields");
        $fieldDiff->addAttribute('Id', 'URL');
        $fieldDiff->addAttribute('Type', 'String');

        $fieldDiff = $this->CUser->addChild("Fields");
        $fieldDiff->addAttribute('Id', 'TIMESTAMP');
        $fieldDiff->addAttribute('Type', 'Int');

        //ibe 1
        $root = $this->createGame($root, "IBE 1", 1, $pdo);
        //IBE 2 NORMAL
        $root = $this->createGame($root, "IBE 2", 1, $pdo);
        //IBE 2 EXTREME
        $root = $this->createGame($root, "IBE 2", 2, $pdo);
        //IBE 1 REVERSE
        $root = $this->createGame($root, "IBE 1 REV", 1, $pdo);
        //CV NORMAL
        $root = $this->createGame($root, "IBE CV", 1, $pdo);
        //CV EXTREME
        $root = $this->createGame($root, "IBE CV", 2, $pdo);
        //CV EZ
        $root = $this->createGame($root, "IBE CV EZ", 1, $pdo);
        //CV PRO NORMAL
        $root = $this->createGame($root, "IBE CV PRO", 1, $pdo);
        //CV PRO EXTREME
        $root = $this->createGame($root, "IBE CV PRO", 2, $pdo);
        //2.1 NORMAL
        $root = $this->createGame($root, "IBE 2.1", 1, $pdo);
        //2.1 EXTREME
        $root = $this->createGame($root, "IBE 2.1", 2, $pdo);
        //BTB
        $root = $this->createGame($root, "BTB", 1, $pdo);
        //BTB PRO
        $root = $this->createGame($root, "BTB PRO", 1, $pdo);
        //BTB BANE
        $root = $this->createGame($root, "BTB BANE", 1, $pdo);

        return new Response($root->asXML(), 200, [
            'Content-Type' => 'text/xml',
        ]);
    }
    public function createGame($root, $game, $diff, $pdo)
    {
        $reqsql = "SELECT
                `run`.`RUN_ID` AS `RUN_ID`,
                `run`.`TIME` AS `TIME`,
                `run`.`TIMESTAMP` AS `TIMESTAMP`,
                `GAME`.`NAME_SHORT` AS `NAME`,
                `DIFFICULTY`.`DIFFICULTY_ID` AS `DIFF`,
                (SELECT
                        GROUP_CONCAT(CONCAT(`pp`.`NAME`,
                                        '/',
                                        CONVERT( `crcr`.`COLOR` USING UTF8))
                                ORDER BY `pp`.`NAME` ASC
                                SEPARATOR '|')
                    FROM
                        ((`PLAYER` `pp`
                        JOIN `CHARACTER` `cc` ON ((`pp`.`PLAYER_ID` = `cc`.`PLAYER_ID`)))
                        JOIN `CHARACTER_RUN` `crcr` ON ((`crcr`.`CHARACTER_ID` = `cc`.`CHARACTER_ID`)))
                    WHERE
                        ((`crcr`.`RUN_ID` = `run`.`RUN_ID`)
                            AND (`pp`.`TYPE` = 'USER')
                            AND (`crcr`.`LEFT` = 0))) AS `PLAYERS`,
                (SELECT
                        COUNT(`pp`.`PLAYER_ID`)
                    FROM
                        ((`PLAYER` `pp`
                        JOIN `CHARACTER` `cc` ON ((`pp`.`PLAYER_ID` = `cc`.`PLAYER_ID`)))
                        JOIN `CHARACTER_RUN` `crcr` ON ((`crcr`.`CHARACTER_ID` = `cc`.`CHARACTER_ID`)))
                    WHERE
                        ((`crcr`.`RUN_ID` = `run`.`RUN_ID`)
                            AND (`pp`.`TYPE` = 'COMPUTER'))) AS `AI`
            FROM
                `RUN` `run`
                JOIN `GAMEMODE` `gm` ON `run`.`GAMEMODE_ID` = `gm`.`GAMEMODE_ID`
                JOIN `REGION` `reg` ON `run`.`REGION_ID` = `reg`.`REGION_ID`
                JOIN `GAME` ON `gm`.`GAME_ID` = `GAME`.`GAME_ID`
                JOIN `DIFFICULTY` ON `DIFFICULTY`.`DIFFICULTY_ID` = `gm`.`DIFFICULTY_ID`
                JOIN `SPEED` ON `SPEED`.`SPEED_ID` = `gm`.`SPEED_ID`
                WHERE GAME.NAME_SHORT='IBE 1' AND `DIFFICULTY`.`DIFFICULTY_ID`=1
            ORDER BY `run`.`TIME`
            LIMIT 10";

        $req = $pdo->prepare($reqsql);
        $req->bindValue(":game", $game);
        $req->bindValue(":diff", $diff);
        $req->execute();
        while ($data = $req->fetch()) {
            $players_str = $data["PLAYERS"];
            $players = explode("|", $players_str);

            $instance = $this->CUser->addChild("Instances");
            $instance->addAttribute('Id', $this->instance_nb);
            $time = $instance->addChild("Fixed");
            $time->addAttribute('Fixed', $data["TIME"]);
            $childTime = $time->addChild("Field");
            $childTime->addAttribute('Id', 'TIME');

            $game_elem = $instance->addChild("String");
            $game_elem->addAttribute('String', $data["NAME"]);
            $childGame = $game_elem->addChild("Field");
            $childGame->addAttribute('Id', 'GAME');
            $diff_elem = $instance->addChild("Int");
            $diff_elem->addAttribute('Int', $data["DIFF"]);
            $childDiff = $diff_elem->addChild("Field");
            $childDiff->addAttribute('Id', 'DIFFICULTY');

            $url = "https://rankings.icebanelingescape.com/escape/" . $data["RUN_ID"];
            $url_elem = $instance->addChild("String");
            $url_elem->addAttribute('String', $url);
            $childDiff = $url_elem->addChild("Field");
            $childDiff->addAttribute('Id', 'URL');

            $timestamp_elem = $instance->addChild("Int");
            $timestamp_elem->addAttribute('Int', $data["TIMESTAMP"]);
            $childDiff = $timestamp_elem->addChild("Field");
            $childDiff->addAttribute('Id', 'TIMESTAMP');

            for ($j = 0; $j < count($players); $j++) {
                $player = $instance->addChild("String");
                $player->addAttribute('String', $players[$j]);
                $childPlayer = $player->addChild("Field");
                $childPlayer->addAttribute('Id', 'PLAYERS');
                if ($j != 0) {
                    $childPlayer->addAttribute('Index', $j);
                }
            }
            $this->instance_nb++;
        }
        return $root;
    }
}
