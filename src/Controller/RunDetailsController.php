<?php

namespace App\Controller;

use Doctrine\DBAL\Driver\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RunDetailsController extends AbstractController
{

    /**
     * @Route("/escape/{run_id}", name="run_details", requirements={"run_id"="\d+"})
     */
    public function index(Connection $pdo, Request $request, $run_id)
    {
        $type = self::getType($pdo, $run_id);
        if ($type === null) {
            throw $this->createNotFoundException();
        }
        $dataRun = self::getRun($pdo, $run_id, $type);
        $videos = self::getVideo($pdo, $run_id);
        $challenges = self::getChallenges($pdo, $run_id);
        $players = self::getPlayers($pdo, $run_id, "USER");
        $ais = self::getPlayers($pdo, $run_id, "COMPUTER");
        return $this->render('run_details/index.html.twig', [
            'dataRun' => $dataRun,
            'videos' => $videos,
            'type' => $type,
            'challenges' => $challenges,
            'players' => $players,
            'ais' => $ais,
        ]);
    }
    private function getType($pdo, $run_id)
    {
        $reqsql_game_type = "SELECT TYPE FROM RUN as r JOIN GAMEMODE as gm On r.GAMEMODE_ID=gm.GAMEMODE_ID JOIN GAME as g ON g.GAME_ID=gm.GAME_ID WHERE RUN_ID=?";
        $req = $pdo->prepare($reqsql_game_type);
        $req->bindParam(1, $run_id);
        $req->execute();
        $data = $req->fetch();
        return $data["TYPE"];
    }
    private function getRun($pdo, $run_id, $type)
    {
        // to be removed...
        if ($type == 'delta' and false) {
            $reqsql = "SELECT  run.RUN_ID,REVIVES,DEATHS,BONUS,STIM,CREEP,RING,ART,REVART,TIMESHIFT,LEVELUPS,STARTED_AT_RT,STARTED_AT_GT,TIMESTAMP,VERSION,CLIENT_VERSION,TIME,GAME.NAME,DIFFICULTY.NAME AS 'DIF_NAME',SPEED.NAME AS 'SPEED_NAME', REPLAY_NAME,ELAPSED_GAME_TIME,REGION.NAME_SHORT,SUBMITED_ON,
            (SELECT
                    GROUP_CONCAT(CONCAT(pp.REAL_NAME,'/',cr.COLOR) SEPARATOR '|')
                FROM
                    PLAYER as pp
                    JOIN `CHARACTER` as c ON pp.PLAYER_ID = c.PLAYER_ID
                    JOIN `CHARACTER_RUN` as hr ON cr.CHARACTER_ID = c.CHARACTER_ID
                    WHERE cr.RUN_ID=run.RUN_ID AND pp.TYPE='USER'
                    ) as PLAYERS,
            (SELECT
                    COUNT(pp.PLAYER_ID)
                FROM
                    PLAYER as pp
                    JOIN `CHARACTER` as c ON pp.PLAYER_ID = c.PLAYER_ID
                    JOIN `CHARACTER_RUN` as cr ON cr.CHARACTER_ID = c.CHARACTER_ID
                    WHERE cr.RUN_ID=run.RUN_ID AND pp.TYPE='COMPUTER'
                    ) as AI,
            (SELECT
                GROUP_CONCAT(v.URL SEPARATOR ' , ')
            FROM
                VIDEO as v
                WHERE v.RUN_ID=run.RUN_ID AND v.PROVIDER='YOUTUBE'
                ) as VIDEOS
          FROM RUN as run
          JOIN GAMEMODE as gm ON run.GAMEMODE_ID = gm.GAMEMODE_ID
          JOIN GAME ON gm.GAME_ID=GAME.GAME_ID
          JOIN DIFFICULTY ON DIFFICULTY.DIFFICULTY_ID=gm.DIFFICULTY_ID
          JOIN SPEED ON SPEED.SPEED_ID=gm.SPEED_ID
          JOIN REGION ON REGION.REGION_ID=run.REGION_ID
          WHERE run.RUN_ID=?
          GROUP BY gm.GAMEMODE_ID
          ORDER BY run.RUN_ID ";
            $req = $pdo->prepare($reqsql);
            $req->bindParam(1, $run_id);
            $req->execute();
        } else {
            $reqsql = "SELECT  run.RUN_ID,
                (SELECT SUM(`LEVEL`) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS LEVELUPS,
                (SELECT SUM(`LEVEL`) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS BONUS,
                (SELECT SUM(REVIVE) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS REVIVES,
                (SELECT SUM(DEATH) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS DEATHS,
                (SELECT SUM(STIM) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS STIM,
                (SELECT SUM(CREEP) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS CREEP,
                (SELECT SUM(RING) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS RING,
                (SELECT SUM(ART) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS ART,
                (SELECT SUM(TIMESHIFT_CREATE) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS TIMESHIFT_CREATE,
                (SELECT SUM(TIMESHIFT_USE) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS TIMESHIFT,
                (SELECT SUM(RING_REVIVE) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS RING_REVIVE,
                (SELECT SUM(ART_REVIVE) FROM CHARACTER_RUN WHERE RUN_ID=? LIMIT 1) AS ART_REVIVE,
                ELAPSED_GAME_TIME,
                STARTED_AT_RT,
                STARTED_AT_GT,
                TIMESTAMP,
                VERSION,
                CLIENT_VERSION,
                TIME,
                GAME.NAME,
                DIFFICULTY.NAME AS 'DIF_NAME',
                SPEED.NAME AS 'SPEED_NAME',
                REPLAY_NAME,
                REGION.NAME_SHORT,
                SUBMITED_ON,
                 (SELECT
                    GROUP_CONCAT(CONCAT(pp.NAME,'<sep>',cr.COLOR,'<sep>',pp.PLAYER_ID) SEPARATOR '<sep2>')
                FROM
                    PLAYER as pp
                    JOIN `CHARACTER` as c ON pp.PLAYER_ID = c.PLAYER_ID
                    JOIN `CHARACTER_RUN` as cr ON cr.CHARACTER_ID = c.CHARACTER_ID
                    WHERE cr.RUN_ID=run.RUN_ID AND pp.TYPE='USER'
                    ) as PLAYERS,
            (SELECT
                    COUNT(pp.PLAYER_ID)
                FROM
                    PLAYER as pp
                    JOIN `CHARACTER` as c ON pp.PLAYER_ID = c.PLAYER_ID
                    JOIN `CHARACTER_RUN` as cr ON cr.CHARACTER_ID = c.CHARACTER_ID
                    WHERE cr.RUN_ID=run.RUN_ID AND pp.TYPE='COMPUTER'
                    ) as AI,
            (SELECT
                GROUP_CONCAT(v.URL SEPARATOR ' , ')
            FROM
                VIDEO as v
                WHERE v.RUN_ID=run.RUN_ID AND v.PROVIDER='YOUTUBE'
                ) as VIDEOS
          FROM RUN as run
          JOIN GAMEMODE as gm ON run.GAMEMODE_ID = gm.GAMEMODE_ID
          JOIN GAME ON gm.GAME_ID=GAME.GAME_ID
          JOIN DIFFICULTY ON DIFFICULTY.DIFFICULTY_ID=gm.DIFFICULTY_ID
          JOIN SPEED ON SPEED.SPEED_ID=gm.SPEED_ID
          JOIN REGION ON REGION.REGION_ID=run.REGION_ID
          WHERE run.RUN_ID=?
          GROUP BY gm.GAMEMODE_ID
          ORDER BY run.RUN_ID ";
            $req = $pdo->prepare($reqsql);
            $req->bindParam(1, $run_id);
            $req->bindParam(2, $run_id);
            $req->bindParam(3, $run_id);
            $req->bindParam(4, $run_id);
            $req->bindParam(5, $run_id);
            $req->bindParam(6, $run_id);
            $req->bindParam(7, $run_id);
            $req->bindParam(8, $run_id);
            $req->bindParam(9, $run_id);
            $req->bindParam(10, $run_id);
            $req->bindParam(11, $run_id);
            $req->bindParam(12, $run_id);
            $req->bindParam(13, $run_id);
            $req->execute();
        }
        $data = $req->fetch();
        $players = $data["PLAYERS"];
        $players_arr = explode("<sep2>", $players);
        $players_str = "";
        foreach ($players_arr as $ppl) {
            $ppl_arr = explode("<sep>", $ppl);
            $name = $ppl_arr[0];
            $color = $ppl_arr[1];
            $ppl_id = $ppl_arr[2];
            $ppl_str = "<a href='profile/$ppl_id' style='color:rgb($color);filter: brightness(0.93);position: relative; font-weight: 600;text-shadow: none;'>$name </a> ";
            $players_str .= $ppl_str;
        }
        $players_str = substr($players_str, 0, -1);
        $ai = $data["AI"];
        if ($ai > 0) {
            $players_str = $players_str . " <span style='color:grey;'> and $ai AI</span>";
        }
        $time = $data["TIME"];
        $date = $data["TIMESTAMP"];
        $video = $data["VIDEOS"];
        $filename = $data["REPLAY_NAME"];
        $hours = floor($time / 3600);
        $minutes = floor(($time / 60) % 60);
        $seconds = $time % 60;
        //$time = $hours_tot > 0 ? "$hours h $minutes min  $seconds sec" : ($minutes > 0 ? "$minutes min  $seconds sec" : "");
        $time = self::secs_to_str($time);

        $data_run = $data;
        $data_run["game"] = $data["NAME"];
        $data_run["diff_name"] = $data["DIF_NAME"];
        $data_run["speed_name"] = $data["SPEED_NAME"];
        $data_run["region"] = $data["NAME_SHORT"];
        $data_run["version"] = $data["VERSION"];
        $data_run["time"] = $time;
        $data_run["players"] = $players_str;
        $data_run["submited_on"] = strtotime($data["SUBMITED_ON"]);
        $data_run["date"] = $date;
        $lvlups = "";
        if ($data["LEVELUPS"] == 0) {
            $lvlups = $data["BONUS"];
        } else {
            $lvlups = $data["LEVELUPS"];
        }

        $data_run["revives"] = $data["REVIVES"];
        $data_run["deaths"] = $data["DEATHS"];
        $data_run["lvlups"] = $data["LEVELUPS"];
        $data_run["stim"] = $data["STIM"];
        $data_run["creep"] = $data["CREEP"];
        $data_run["ring"] = $data["RING"];
        $data_run["art"] = $data["ART"];
        $data_run["filename"] = $data["REPLAY_NAME"];
        return $data_run;
    }
    private function getPlayers($pdo, $run_id, $type)
    {
        $reqsql_players = "SELECT *,
            p.PLAYER_ID AS PPL,
            (SELECT COUNT(DISTINCT MAP_ID)
                FROM RUN_MAP as rmrm
                JOIN RUN_MAP_COMPLETE as rmcrmc on rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID
                WHERE rmcrmc.CONTROLLED_BY=c.CHARACTER_ID AND rmrm.RUN_ID=?) AS CHALLENGES
            FROM PLAYER as p
            JOIN `CHARACTER` as c ON p.PLAYER_ID=c.PLAYER_ID
            JOIN CHARACTER_RUN as cr ON c.CHARACTER_ID=cr.CHARACTER_ID
            WHERE RUN_ID=? AND p.TYPE='$type'";
        $req_players = $pdo->prepare($reqsql_players);
        $req_players->bindParam(1, $run_id);
        $req_players->bindParam(2, $run_id);
        $req_players->execute();
        $data = $req_players->fetchAll();
        return $data;
    }
    private function getChallenges(Connection $pdo, $run_id)
    {
        $reqsql_challenges = "SELECT
		group_concat(DISTINCT rm.TIME) AS CHAL_TIME,
		group_concat( DISTINCT rm.RUN_MAP_ID) AS CHAL_ID,
		 group_concat(DISTINCT m.MAP_NUMBER) AS CHAL_NUMBER,
		 group_concat(DISTINCT m.MAP_NAME) AS CHAL_NAME,
		 group_concat(DISTINCT CONCAT(p.NAME,'<sep>',cr.COLOR) SEPARATOR ' <sep2> ') AS PLAYER_NAMES,
		 group_concat(DISTINCT g.GAME_ID) AS GAME_ID,
         group_concat(DISTINCT g.CODE_NAME) AS GAME_CODE,
            (SELECT MIN(rmrm.TIME) FROM RUN_MAP as rmrm JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=gm.DIFFICULTY_ID AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190))  ) AS BEST_TIME ,
            (SELECT GROUP_CONCAT(rmrm.TIME ORDER BY rmrm.TIME SEPARATOR ' , ') FROM RUN_MAP as rmrm JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=gm.DIFFICULTY_ID AND gmgm.SPEED_ID=1 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190))  ) AS TIMES_NORMAL ,
            (SELECT GROUP_CONCAT(rmrm.TIME ORDER BY rmrm.TIME SEPARATOR ' , ') FROM RUN_MAP as rmrm JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=gm.DIFFICULTY_ID AND gmgm.SPEED_ID=2 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190))  ) AS TIMES_FAST,
            (SELECT GROUP_CONCAT(rmrm.TIME ORDER BY rmrm.TIME SEPARATOR ' , ')  FROM RUN_MAP as rmrm JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=gm.DIFFICULTY_ID AND gmgm.SPEED_ID=3 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190)) ) AS TIMES_SPEEDY
        FROM RUN_MAP as rm
        JOIN MAP as m ON rm.MAP_ID = m.MAP_ID
        JOIN RUN_MAP_COMPLETE as rmc on rmc.RUN_MAP_ID=rm.RUN_MAP_ID
        LEFT JOIN `CHARACTER` as c ON rmc.CONTROLLED_BY=c.CHARACTER_ID
        LEFT JOIN PLAYER as p ON c.PLAYER_ID=p.PLAYER_ID
        JOIN RUN as r ON r.RUN_ID=rm.RUN_ID
        JOIN CHARACTER_RUN as cr ON r.RUN_ID=cr.RUN_ID AND cr.CHARACTER_ID=c.CHARACTER_ID
        JOIN GAMEMODE as gm ON r.GAMEMODE_ID=gm.GAMEMODE_ID
        JOIN GAME as g ON g.GAME_ID=gm.GAME_ID
        WHERE r.RUN_ID=?
        GROUP BY rm.MAP_ID
        ORDER BY rm.ORDER";
        $req_challenges = $pdo->prepare($reqsql_challenges);
        $req_challenges->bindParam(1, $run_id);
        $req_challenges->execute();
        $challenge_index = 1;
        $challenges = [];
        while ($data_challenges = $req_challenges->fetch()) {
            $challenge = [];

            $reqsql_bonus = "SELECT * FROM RUN_MAP_POWERUP as pwr
            JOIN `CHARACTER` as c ON pwr.CONTROLLED_BY=c.CHARACTER_ID
            JOIN RUN_MAP as rm ON rm.RUN_MAP_ID=pwr.RUN_MAP_ID
            JOIN `CHARACTER_RUN` as cr ON cr.CHARACTER_ID=c.CHARACTER_ID AND cr.RUN_ID=rm.RUN_ID
            JOIN PLAYER as p ON c.PLAYER_ID=p.PLAYER_ID
            WHERE pwr.RUN_MAP_ID=?";
            $req_bonus = $pdo->prepare($reqsql_bonus);
            $req_bonus->bindParam(1, $data_challenges["CHAL_ID"]);
            $req_bonus->execute();
            $bonus_str = "";
            while ($data_bonus = $req_bonus->fetch()) {
                $name = $data_bonus["NAME"];
                $color = $data_bonus["COLOR"];
                $ppl_str = "<span style='color:rgb($color);filter: brightness(0.93);position: relative; font-weight: 600;text-shadow: none;'>$name </span> ";
                $bonus_str .= $ppl_str;
            }
            $bonus_str = substr($bonus_str, 0, -1);

            $reqsql_btn = "SELECT * FROM RUN_MAP_BUTTON as btn
            JOIN `CHARACTER` as c ON btn.CONTROLLED_BY=c.CHARACTER_ID
            JOIN RUN_MAP as rm ON rm.RUN_MAP_ID=btn.RUN_MAP_ID
            JOIN `CHARACTER_RUN` as cr ON cr.CHARACTER_ID=c.CHARACTER_ID AND cr.RUN_ID=rm.RUN_ID
            JOIN PLAYER as p ON c.PLAYER_ID=p.PLAYER_ID
            WHERE btn.RUN_MAP_ID=?";
            $req_btn = $pdo->prepare($reqsql_btn);
            $req_btn->bindParam(1, $data_challenges["CHAL_ID"]);
            $req_btn->execute();
            $btnInfo = "";
            while ($data_btn = $req_btn->fetch()) {
                $name = $data_btn["NAME"];
                $color = $data_btn["COLOR"];
                $ppl_str = "<span style='color:rgb($color);filter: brightness(0.93);position: relative; font-weight: 600;text-shadow: none;'>$name </span> ";
                $btnInfo .= $ppl_str;
            }

            $challenge["index"] = $challenge_index++;
            $challenge["challengeName"] = 'L' . str_pad($data_challenges["CHAL_NUMBER"], 2, '0', STR_PAD_LEFT) . " " . $data_challenges["CHAL_NAME"];
            if ($data_challenges["CHAL_NUMBER"] == 0) {
                $players_str = "EVERYONE";
            } else {
                $players = $data_challenges["PLAYER_NAMES"];
                $players_arr = explode("<sep2>", $players);
                $players_str = "";
                foreach ($players_arr as $ppl) {
                    $ppl_arr = explode("<sep>", $ppl);
                    $name = $ppl_arr[0];
                    $color = $ppl_arr[1];
                    $ppl_str = "<span style='color:rgb($color);filter: brightness(0.93);position: relative; font-weight: 600;text-shadow: none;'>$name </span> ";
                    $players_str .= $ppl_str;
                }
                $players_str = substr($players_str, 0, -1);
            }

            $challenge["playerName"] = $players_str;

            $challenge["time"] = $data_challenges["CHAL_TIME"];
            $challenge["bestTime"] = $data_challenges["BEST_TIME"];

            $timesNormal = $data_challenges["TIMES_NORMAL"];
            if ($timesNormal != null) {
                $timesNormal = explode(',', $timesNormal);
                $timesNormal = array_slice($timesNormal, 0, 5, true);
                $challenge["timesNormal"] = $timesNormal;
            } else {
                $challenge["timesNormal"] = [];
            }
            $timesFast = $data_challenges["TIMES_FAST"];
            if ($timesFast != null) {
                $timesFast = explode(',', $timesFast);
                $timesFast = array_slice($timesFast, 0, 5, true);
                $challenge["timesFast"] = $timesFast;
            } else {
                $challenge["timesFast"] = [];
            }
            $timesSpeedy = $data_challenges["TIMES_SPEEDY"];
            if ($timesSpeedy != null) {
                $timesSpeedy = explode(',', $timesSpeedy);
                $timesSpeedy = array_slice($timesSpeedy, 0, 5, true);
                $challenge["timesSpeedy"] = $timesSpeedy;
            } else {
                $challenge["timesSpeedy"] = [];
            }

            $challenge["bonus"] = $bonus_str;
            $challenge["btn"] = $btnInfo;
            if ($data_challenges["GAME_ID"] == 1) {
                $img_name = "1_" . $data_challenges["CHAL_NUMBER"] . ".jpg";
            } elseif ($data_challenges["GAME_ID"] == 2) {
                $img_name = "2_" . $data_challenges["CHAL_NUMBER"] . ".jpg";
            } elseif ($data_challenges["GAME_ID"] == 3) {
                $img_name = "21_" . $data_challenges["CHAL_NUMBER"] . ".jpg";
            } elseif ($data_challenges["GAME_ID"] == 4) {
                $img_name = "cv_" . $data_challenges["CHAL_NUMBER"] . ".jpg";
            } elseif ($data_challenges["GAME_ID"] == 5) {
                $img_name = "cv-ez_" . $data_challenges["CHAL_NUMBER"] . ".jpg";
            } elseif ($data_challenges["GAME_ID"] == 6) {
                $img_name = "cv-pro_" . $data_challenges["CHAL_NUMBER"] . ".jpg";
            } elseif ($data_challenges["GAME_ID"] == 11) {
                $img_name = "1_" . $data_challenges["CHAL_NUMBER"] . ".jpg";
            } else {
                $img_name = strtolower($data_challenges['GAME_CODE']) . "_" . $data_challenges["CHAL_NUMBER"] . ".jpg";
            }
            $challenge["image"] = $img_name;
            array_push($challenges, $challenge);
        }
        return $challenges;
    }
    private function getVideo($pdo, $run_id)
    {
        $reqsql = "SELECT URL FROM VIDEO WHERE RUN_ID=? AND PROVIDER='YOUTUBE'";
        $req = $pdo->prepare($reqsql);
        $req->bindParam(1, $run_id);
        $req->execute();
        $data = $req->fetchAll();
        return $data;
    }
    private function secs_to_str($duration)
    {
        $periods = array(
            'day' => 86400,
            'hour' => 3600,
            'minute' => 60,
            'second' => 1,
        );

        $parts = array();

        foreach ($periods as $name => $dur) {
            $div = floor($duration / $dur);

            if ($div == 0) {
                continue;
            } elseif ($div == 1) {
                $parts[] = $div . " " . $name;
            } else {
                $parts[] = $div . " " . $name . "s";
            }

            $duration %= $dur;
        }

        $last = array_pop($parts);

        if (empty($parts)) {
            return $last;
        } else {
            return join(', ', $parts) . " and " . $last;
        }
    }
}
