<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\BaseController;

class EscapesController extends BaseController
{
    /**
     * @Route("/escapes", name="escapes")
     */
    public function index(AjaxController $ax)
    {
        
        $gameList = $this->conn->query('SELECT * FROM `GAME` ORDER BY `ORDER` ASC')->fetchAll();
        return $this->render('escapes/index.html.twig', [
            'gameList' => $gameList
        ]);
    }
}
