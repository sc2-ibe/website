<?php

namespace App\Controller;

use Doctrine\DBAL\Driver\Connection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MapsController extends AbstractController
{
    /**
     * @Route("/maps", name="maps")
     */
    public function index()
    {
        return $this->render('maps/index.html.twig', [
            'controller_name' => 'MapsController',
        ]);
    }

    public function findMaps(Connection $pdo, Request $request)
    {
        $reqsql = "
		SELECT *,
		(SELECT  GROUP_CONCAT( DISTINCT CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=1 AND gmgm.SPEED_ID=1 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190))AND TYPE = 'USER'  ) AS TIMES_NORMAL_NORMAL ,
		(SELECT  GROUP_CONCAT( DISTINCT CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=1 AND gmgm.SPEED_ID=2 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190))AND TYPE = 'USER' ) AS TIMES_NORMAL_FAST,
		(SELECT  GROUP_CONCAT( DISTINCT CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=1 AND gmgm.SPEED_ID=3 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190))AND TYPE = 'USER' ) AS TIMES_NORMAL_SPEEDY,
		(SELECT  GROUP_CONCAT( DISTINCT CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=2 AND gmgm.SPEED_ID=1 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190) )AND TYPE = 'USER'  ) AS TIMES_EXT_NORMAL ,
		(SELECT  GROUP_CONCAT( DISTINCT CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=2 AND gmgm.SPEED_ID=2 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190))AND TYPE = 'USER' ) AS TIMES_EXT_FAST,
		(SELECT  GROUP_CONCAT( DISTINCT CONCAT(rmrm.TIME,'<sep>',pp.NAME,'<sep>',rmrm.RUN_ID,'<sep>',crcr.COLOR) ORDER BY rmrm.TIME SEPARATOR '<sep3>') FROM RUN_MAP as rmrm JOIN RUN_MAP_COMPLETE as rmcrmc ON rmrm.RUN_MAP_ID=rmcrmc.RUN_MAP_ID JOIN RUN as rr ON rr.RUN_ID=rmrm.RUN_ID  JOIN CHARACTER_RUN as crcr on rr.RUN_ID=crcr.RUN_ID and rmcrmc.CONTROLLED_BY=crcr.CHARACTER_ID JOIN GAMEMODE as gmgm ON gmgm.GAMEMODE_ID=rr.GAMEMODE_ID JOIN `CHARACTER` as cc ON cc.CHARACTER_ID=rmcrmc.CONTROLLED_BY JOIN PLAYER as pp on pp.PLAYER_ID=cc.PLAYER_ID WHERE MAP_ID=m.MAP_ID AND gmgm.DIFFICULTY_ID=2 AND gmgm.SPEED_ID=3 AND NOT (rr.BUILD>=38215 AND rr.VERSION<55 AND MAP_ID=172) AND NOT (rr.VERSION<9 AND MAP_ID IN (189,190))AND TYPE = 'USER' ) AS TIMES_EXT_SPEEDY
         FROM MAP as m JOIN GAME as g ON m.GAME_ID=g.GAME_ID";
        $first = 1;
        $req = $pdo->prepare($reqsql);
        $req->execute();
        $result = [
            'data' => [],
        ];
        while ($data = $req->fetch()) {
            $colValues = [];

            $colValues['game'] = $data["NAME_SHORT"];

            if ($data["GAME_ID"] == 1) {
                $img_name = "1_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 2) {
                $img_name = "2_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 3) {
                $img_name = "21_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 4) {
                $img_name = "cv_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 5) {
                $img_name = "cv-ez_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 6) {
                $img_name = "cv-pro_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 11) {
                $img_name = "1_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 12) {
                $img_name = "btb_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 13) {
                $img_name = "btb-pro_" . $data["MAP_NUMBER"] . ".jpg";
            } elseif ($data["GAME_ID"] == 14) {
                $img_name = "btb-bane_" . $data["MAP_NUMBER"] . ".jpg";
            }

            $timesNN = $data["TIMES_NORMAL_NORMAL"];
            if ($timesNN != null) {
                $timesNN = explode('<sep3>', $timesNN);
                $timesNN = array_slice($timesNN, 0, 20, true);
            } else {
                $timesNN = [];
            }
            $timesEN = $data["TIMES_EXT_NORMAL"];
            if ($timesEN != null) {
                $timesEN = explode('<sep3>', $timesEN);
                $timesEN = array_slice($timesEN, 0, 20, true);
            } else {
                $timesEN = [];
            }
            $timesNF = $data["TIMES_NORMAL_FAST"];
            if ($timesNF != null) {
                $timesNF = explode('<sep3>', $timesNF);
                $timesNF = array_slice($timesNF, 0, 20, true);
            } else {
                $timesNF = [];
            }
            $timesEF = $data["TIMES_EXT_FAST"];
            if ($timesEF != null) {
                $timesEF = explode('<sep3>', $timesEF);
                $timesEF = array_slice($timesEF, 0, 20, true);
            } else {
                $timesEF = [];
            }
            $timesNS = $data["TIMES_NORMAL_SPEEDY"];
            if ($timesNS != null) {
                $timesNS = explode('<sep3>', $timesNS);
                $timesNS = array_slice($timesNS, 0, 20, true);
            } else {
                $timesNS = [];
            }
            $timesES = $data["TIMES_EXT_SPEEDY"];
            if ($timesES != null) {
                $timesES = explode('<sep3>', $timesES);
                $timesES = array_slice($timesES, 0, 20, true);
            } else {
                $timesES = [];
            }
            $bestTime = "";
            if (count($timesNN) > 0) {
                //$bestTime .= "<span class='span-best-times'>Normal Normal: <br/>";
                $bestTime .= "<div class='span-best-times'>Normal Normal: <br/>";
                $index = 1;
                foreach ($timesNN as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }
                $bestTime .= "</div>";
            }
            if (count($timesNF) > 0) {
                $bestTime .= "<div class='span-best-times'>Normal Fast: <br/>";
                $index = 1;

                foreach ($timesNF as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }
                $bestTime .= "</div>";
            }
            if (count($timesNS) > 0) {
                $bestTime .= "<div class='span-best-times'>Normal Speedy: <br/>";
                $index = 1;

                foreach ($timesNS as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }
                $bestTime .= "</div>";
            }
            if (count($timesEN) > 0) {
                $bestTime .= "<div class='span-best-times'> Extreme Normal: <br/>";
                $index = 1;

                foreach ($timesEN as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                    //$bestTime .= "<p></p>";
                }
                $bestTime .= "</div>";
            }
            if (count($timesEF) > 0) {
                $bestTime .= "<div class='span-best-times'>Extreme fast: <br/>";
                $index = 1;

                foreach ($timesEF as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }
                $bestTime .= "</div>";
            }
            if (count($timesES) > 0) {
                $bestTime .= "<div class='span-best-times'>Extreme Speedy:<br/>";
                $index = 1;

                foreach ($timesES as $time) {
                    $time = explode('<sep>', $time);
                    $bestTime .= "<p><a class='a-best-time' href='escape/" . $time[2] . "'>" . $index++ . ") " . self::secs_to_str($time[0]) . "<span style='color:rgb(" . $time[3] . ");' class='player-name'> (" . $time[1] . ")</span></a> </p>";
                }

                $bestTime .= "</div>";
            }
            $img = "<img alt='" . $data["MAP_NAME"] . "'class='img-fluid challenge-img' onclick='enlargeImg(this.src)' src='/uploads/img/maps/thumb/" . $img_name . "' />";
            $colValues['challenge'] = $data["MAP_NUMBER"] . ". " . $data["MAP_NAME"];
            $colValues['bestTime'] = htmlspecialchars_decode($bestTime);
            $colValues['img'] = $img;

            $result['data'][] = $colValues;
        }
        return new JsonResponse($result);
    }
    private function secs_to_str($duration)
    {
        $seconds = $duration;
        $hours = floor($seconds / 3600);
        $minutes = floor(($seconds / 60) % 60);
        $seconds = $seconds % 60;
        $ms = $duration - floor($duration);

        $formattedTime = [];
        if ($hours > 0) {
            $formattedTime[] = "{$hours}:";
        }

        if ($minutes >= 0) {
            $formattedTime[] = str_pad($minutes, 2, '0', STR_PAD_LEFT) . ":";
        }

        if ($seconds >= 0) {
            $formattedTime[] = str_pad($seconds, 2, '0', STR_PAD_LEFT) . ($hours <= 0 ? "'<small>" . str_pad((round($ms, 2) * 100), 2, '0', STR_PAD_LEFT) . "</small>" : "") . "";
            // . ($hours <= 0 ? "<small>," . (round($ms, 1) * 10) . "</small>" : "") . "<small>s</small>";
        }

        $formattedTime = implode('', $formattedTime);
        return $formattedTime;
    }
}
