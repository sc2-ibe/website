<?php

namespace App\Controller;

use PDO;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\DBAL\Driver\Connection;

class StatsLobbiesController extends AbstractController
{


    /**
     * @var Connection
     */
    protected $conn;



    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @Route("/stats/lobbies", name="stats_lobbies")
     */
    public function index()
    {
        $stat = self::findStatsLobbies();
        $ibe1 = array_column($stat, 'ibe1');
        $rev_ibe1 = array_column($stat, 'rev_ibe1');
        $ibe2 = array_column($stat, 'ibe2');
        $ibe2_1 = array_column($stat, 'ibe2_1');
        $ibe_cv = array_column($stat, 'ibe_cv');
        $ibe_cv_ez = array_column($stat, 'ibe_cv_ez');
        $ibe_cv_pro = array_column($stat, 'ibe_cv_pro');
        $btb = array_column($stat, 'btb');
        $btb_ez = array_column($stat, 'btb');
        $btb_pro = array_column($stat, 'btb_pro');
        $btb_bane = array_column($stat, 'btb_bane');
        $day = array_column($stat, 'day');
        return $this->render('stats_lobbies/index.html.twig', [
            'stats' => $stat,
            'ibe1' =>  $ibe1,
            'rev_ibe1' =>  $rev_ibe1,
            'ibe2' =>  $ibe2,
            'ibe2_1' =>  $ibe2_1,
            'ibe_cv' =>  $ibe_cv,
            'ibe_cv_ez' =>  $ibe_cv_ez,
            'ibe_cv_pro' =>  $ibe_cv_pro,
            'btb' =>  $btb,
            'btb_ez' =>  $btb_ez,
            'btb_pro' =>  $btb_pro,
            'btb_bane' =>  $btb_bane,
            'day' =>  $day,
        ]);
    }
    public function findStatsLobbies()
    {

        $reqsql = "
                        SELECT 
                            SUM(
                                CASE WHEN 
                                    status='started' and 
                                    map_document_version_id in
                                        (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Ice Baneling Escape') 
                                THEN 1 ELSE 0 END) 
                            as ibe1
                            , SUM(
                                CASE WHEN 
                                    status='started' and 
                                    map_document_version_id in
                                        (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Reverse Ice Baneling Escape') 
                                THEN 1 ELSE 0 END) 
                            as rev_ibe1
                            ,
                                SUM(
                                    CASE WHEN 
                                        status='started' and 
                                        map_document_version_id in
                                            (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Ice Baneling Escape 2') 
                                    THEN 1 ELSE 0 END) 
                            as ibe2,
                                SUM(
                                    CASE WHEN 
                                        status='started' and 
                                        map_document_version_id in
                                            (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Ice Baneling Escape 2.1 - The Ice Awakens') 
                                    THEN 1 ELSE 0 END) 
                            as ibe2_1,
                            
                                SUM(
                                    CASE WHEN 
                                        status='started' and 
                                        map_document_version_id in
                                            (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Ice Baneling Escape - Cold Voyage') 
                                    THEN 1 ELSE 0 END) 
                            as ibe_cv
                            ,
                                SUM(
                                    CASE WHEN 
                                        status='started' and 
                                        map_document_version_id in
                                            (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Ice Baneling Escape - EZ') 
                                    THEN 1 ELSE 0 END) 
                            as ibe_cv_ez
                            ,
                                SUM(
                                    CASE WHEN 
                                        status='started' and 
                                        map_document_version_id in
                                            (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Ice Baneling Escape - Pro') 
                                    THEN 1 ELSE 0 END) 
                            as ibe_cv_pro,
                                SUM(
                                    CASE WHEN 
                                        status='started' and 
                                        map_document_version_id in
                                            (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Back to Brood Ice Escape') 
                                    THEN 1 ELSE 0 END) 
                            as btb
                            , SUM(
                                    CASE WHEN 
                                        status='started' and 
                                        map_document_version_id in
                                            (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Back to Brood Ice Escape PRO') 
                                    THEN 1 ELSE 0 END) 
                            as btb_pro
                            , SUM(
                                    CASE WHEN 
                                        status='started' and 
                                        map_document_version_id in
                                            (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Back to Brood Baneling mode') 
                                    THEN 1 ELSE 0 END) 
                            as btb_bane
                            , SUM(
                                    CASE WHEN 
                                        status='started' and 
                                        map_document_version_id in
                                            (SELECT dv.id FROM sc2arcade_v2.s2_document_version dv inner join sc2arcade_v2.s2_document as d on d.id = dv.document_id WHERE d.name = 'Back to Brood Ice Escape EZ') 
                                    THEN 1 ELSE 0 END) 
                            as btb_ez
                            ,
                            CAST(l.created_at AS DATE) as day
                        FROM sc2arcade_v2.s2_game_lobby as l
                        INNER JOIN sc2arcade_v2.s2_document_version dv ON dv.id = l.map_document_version_id
						INNER JOIN sc2arcade_v2.s2_document d ON d.id = dv.document_id
                        where
                        d.name in (
							'Ice Baneling Escape',
							'Ice Baneling Escape 2',
							'Ice Baneling Escape 2.1 - The Ice Awakens',
							'Ice Baneling Escape - Cold Voyage',
							'Ice Baneling Escape - EZ',
							'Ice Baneling Escape - Pro',
							'Reverse Ice Baneling Escape',
							'Ice Baneling Escape - Cold Voyage',
							'Back to Brood Ice Escape',
                            'Back to Brood Ice Escape PRO',
							'Back to Brood Baneling mode',
							'Back to Brood Ice Escape EZ')
                        and closed_at IS NOT NULL
                        GROUP BY CAST(l.created_at AS DATE)
                        ORDER BY CAST(l.created_at AS DATE) asc;";

        $req = $this->conn->prepare($reqsql);
        $req->execute();
        $data = $req->fetchAll();

        return $data;
    }
}
