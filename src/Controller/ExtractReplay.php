<?php

namespace App\Controller;

use App\BaseController;
use Carbon\Carbon;
use Carbon\CarbonInterval;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class ExtractReplay extends BaseController
{
    /**
     * @var Connection
     */
    protected $conn;

    /**
     * @var Filesystem
     */
    protected $fs;

    public function __construct(Connection $conn)
    {
        $this->conn = $conn;
        $this->fs = new Filesystem();
    }

    public function extractReplay(Request $request)
    {
        $file = $request->files->get('file');
        /** @var UploadedFile|null $file */

        // no file uploaded
        if ($file === null) {
            return new JsonResponse([
                'success' => false,
                'message' => 'Missing file, huh?',
            ]);
        }

        // fetch results from processing of particular file from `REPLAY_FILE` which acts as a cache table
        // so we won't waste CPU cycles on processing the same files over and over
        $fileHash = md5_file($file->getPathname());
        $replayFile = $this->fetchReplayByFileHash($fileHash);
        if ($replayFile !== false) {
            if ($replayFile['RUN_ID'] !== null) {
                
                // accept file nonetheless if it's missing on our end - NOT TESTED!
                // START
                $absName = $this->getReplayAbsolutePath($replayFile['REPLAY_NAME']);
                if (!$this->fs->exists($absName)) {
                    $file->move($this->getParameter('app.uploads_dir.replays'), $replayFile['REPLAY_NAME']);
                }
                // END
                
                return new JsonResponse([
                    'success' => true,
                    'message' => sprintf('This replay has already been uploaded (ID = %d).', $replayFile['RUN_ID']),
                    'path' => $this->generateUrl('run_details', ['run_id' => $replayFile['RUN_ID']], UrlGeneratorInterface::ABSOLUTE_PATH),
                ]);
            } else {
                return new JsonResponse([
                    'success' => false,
                    'message' => sprintf('An error occurred during analysis of the replay. It has been stored in the database for further investigation. Report ID: %s', $fileHash),
                ]);
            }
        }

        try {
            $gminfo = $this->extract($file->getPathname());
        } catch (ProcessFailedException $e) {
            $process = $e->getProcess();
            /** @var Process $process */

            $this->updateReplayFileHash(
                $fileHash,
                $file->getClientOriginalName(),
                $process->getExitCode(),
                null,
                $process->getErrorOutput(),
                $process->getOutput()
            );

            switch ($process->getExitCode()) {
                    // not supported
                case 2: {
                        return new JsonResponse([
                            'success' => false,
                            'message' => 'Replay file damaged/corrupted. Likely played on too old SC2 version.',
                        ]);
                        break;
                    }

                    // internal error
                default: {
                        $filename = $fileHash . '.SC2Replay';
                        $file->move($this->getParameter('app.uploads_dir.err_replays'), $filename);
                        return new JsonResponse([
                            'success' => false,
                            'message' => sprintf('An error occurred during analysis of the replay. It has been stored in the database for further investigation. Report ID: %s', $fileHash),
                        ]);
                        break;
                    }
            }
        }

        $table = $this->submitReplay($gminfo, $file->getPathname(), $file->getClientOriginalName());
        return new JsonResponse($table);
    }

    public function submitReplay($gminfo, $pathname, $originalFileName)
    {
        // not an IBE map
        if (!$gminfo['map']) {
            return [
                'success' => false,
                'message' => sprintf(
                    'Not supported map "%s".',
                    $gminfo['general']['game_title']
                ),
            ];
        }

        //
        if ($gminfo["map"]["id"] == "IBE2.1") {
            if (array_key_exists("schema_version", $gminfo["result"]) == false) {
                return [
                    'success' => false,
                    'message' => 'That looks like a replay from an old version of IBE 2.1? Only replays after 2019/02/09 are supported.',
                ];
            }
        }
        if ($gminfo['general']['resumed_replay']) {
            return [
                'success' => false,
                'message' => 'It appears this replay has been resumed. Segmented runs are against the rules.',
            ];
        }

        // no escape recorded
        if (!$gminfo['result']) {
            return [
                'success' => false,
                'message' => 'This replay is from a failed run.',
            ];
        }

        $fileHash = md5_file($pathname);
        $runInfo = $this->fetchRunFromResult($gminfo);
        if ($runInfo !== false) {
            $this->updateReplayFileHash(
                $fileHash,
                $originalFileName,
                0,
                $runInfo['RUN_ID'],
                null,
                json_encode($gminfo)
            );

            return [
                'success' => true,
                'message' => sprintf('This replay has already been uploaded (ID = %d).', $runInfo['RUN_ID']),
                'path' => $this->generateUrl('run_details', ['run_id' => $runInfo['RUN_ID']], UrlGeneratorInterface::ABSOLUTE_PATH),
            ];
        }

        if ($this->isReplayCheated($gminfo)) {
            return [
                'success' => false,
                'message' => 'This replay has been invalidated by moderators.',
            ];
        }

        $gameMode = $this->getGameMode($gminfo['map'], $gminfo['result']);
        if ($gameMode === false) {
            return [
                'success' => false,
                'message' => 'Unknown game mode / not supported map.',
            ];
        }

        $table = $this->storeValidRun($gminfo, $gameMode, $pathname);

        if ($table['success']) {
            $this->updateReplayFileHash(
                $fileHash,
                $originalFileName,
                0,
                $table['run_id'],
                null,
                json_encode($gminfo)
            );
        }

        $this->triggerWebhook($table['run_id']);

        return $table;
    }

    public function triggerWebhook($runId)
    {
        if ($this->getParameter('app.discord.webhook_url') == false) return;

        $msg = $this->generateUrl('run_details', ['run_id' => $runId], UrlGeneratorInterface::ABSOLUTE_URL);
        $payload = json_encode([
            'content' => $msg
        ], JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

        $ch = curl_init($this->getParameter('app.discord.webhook_url'));
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-type: application/json',
            'Content-length: ' . strlen($payload)
        ]);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        // response is in json:
        // $response = '{"message": "Cannot send an empty message", "code": 50006}';
    }

    public function storeValidRun($gminfo, $gameMode, $pathname)
    {
        $repFilename = $this->generateFilename($gminfo);
        $this->fs->copy($pathname, implode('/', [$this->getParameter('app.uploads_dir.replays'), $repFilename]));

        $table = $this->extractCV($gminfo, $repFilename, $gameMode['GAMEMODE_ID']);

        if (isset($table['run_id'])) {
            $table['path'] = $this->generateUrl('run_details', ['run_id' => $table['run_id']], UrlGeneratorInterface::ABSOLUTE_PATH);
        }

        $table['success'] = true;
        $table['message'] = $this->generateFilename($gminfo, false);

        return $table;
    }

    public function fetchReplayByFileHash($fileHash)
    {
        $stmt = $this->conn->prepare('SELECT * FROM REPLAYFILE WHERE FILE_HASH = ?');
        $stmt->bindValue(1, $fileHash);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function deleteReplayByFileHash($fileHash)
    {
        $stmt = $this->conn->prepare('DELETE FROM REPLAYFILE WHERE FILE_HASH = ?');
        $stmt->bindValue(1, $fileHash);
        $stmt->execute();

        $absName = $this->getErrReplayAbsolutePath($fileHash . '.SC2Replay');
        $this->fs->remove($absName);
    }

    public function updateReplayFileHash($fileHash, $fileName = null, $resultCode = 0, $runId = null, $stdErr = null, $stdOut = null)
    {
        $this->deleteReplayByFileHash($fileHash);

        $stmt = $this->conn->prepare('INSERT INTO REPLAYFILE(FILE_HASH,FILE_NAME,RESULT_CODE,RUN_ID,STDERR,STDOUT) VALUES(?,?,?,?,?,?)');
        $stmt->bindValue(1, $fileHash);
        $stmt->bindValue(2, $fileName);
        $stmt->bindValue(3, $resultCode);
        $stmt->bindValue(4, $runId);
        $stmt->bindValue(5, $stdErr);
        $stmt->bindValue(6, $stdOut);
        $stmt->execute();
    }

    public function generateFilename($gminfo, $includeExtension = true)
    {
        $humanPlayers = array_filter($gminfo['general']['player_slots'], function ($pslot) {
            return $pslot['type'] === 'USER';
        });

        $filename = implode(' ', [
            // map id
            $gminfo['map']['id'],
            // prettified escape time
            CarbonInterval::seconds(floor($gminfo['result']['escape_time']))->cascade()->forHumans(['join' => ''], true),
            // name of each player from that game
            implode(' ', array_map(
                function ($pslot) {
                    return $pslot['name'];
                },
                $humanPlayers
            )),
            // date of game
            Carbon::createFromTimestampUTC($gminfo['general']['timestamp'])->toDateTimeString(),
        ]);
        $filename = str_replace(":", "_", $filename);
        if ($includeExtension) {
            $filename .= '.SC2Replay';
        }

        return $filename;
    }

    public function getReplayAbsolutePath($filename)
    {
        return $this->getParameter('app.uploads_dir.replays') . '/' . $filename;
    }

    public function getErrReplayAbsolutePath($filename)
    {
        return $this->getParameter('app.uploads_dir.err_replays') . '/' . $filename;
    }

    public function extract($filename, $flags = [])
    {
        $rpath = $this->getParameter('app.ibe_repdump_dir');
        if (!preg_match('/^(\/|\\\)/', $this->getParameter('app.ibe_repdump_dir'))) {
            $rpath = $this->getParameter('kernel.project_dir') . '/' . $rpath;
        }
        $process = new Process(array_merge([
            $this->getParameter('app.python_runner'),
            $rpath,
            '--verbose',
            $filename,
        ], $flags));
        $process->setTimeout(300);
        $process->run();

        // executes after the command finishes
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return json_decode($process->getOutput(), true);
    }

    public function removeRunDetails($runId)
    {
        $this->conn->prepare('DELETE FROM `RUN_MAP_POWERUP` WHERE `RUN_MAP_ID` IN (
            SELECT `RUN_MAP_ID` FROM `RUN_MAP` WHERE `RUN_ID` = :id
        )')->execute([
            'id' => $runId,
        ]);
        $this->conn->prepare('DELETE FROM `RUN_MAP_COMPLETE` WHERE `RUN_MAP_ID` IN (
            SELECT `RUN_MAP_ID` FROM `RUN_MAP` WHERE `RUN_ID` = :id
        )')->execute([
            'id' => $runId,
        ]);
        $this->conn->prepare('DELETE FROM `RUN_MAP_BUTTON` WHERE `RUN_MAP_ID` IN (
            SELECT `RUN_MAP_ID` FROM `RUN_MAP` WHERE `RUN_ID` = :id
        )')->execute([
            'id' => $runId,
        ]);
        $this->conn->prepare('DELETE FROM RUN_MAP WHERE RUN_ID = :id')->execute([
            'id' => $runId,
        ]);
        $this->conn->prepare('DELETE FROM CHARACTER_RUN WHERE RUN_ID = :id')->execute([
            'id' => $runId,
        ]);
    }

    public function removeRun($runId)
    {
        $this->removeRunDetails($runId);
        $this->conn->prepare('DELETE FROM RUN WHERE RUN_ID = :id')->execute([
            'id' => $runId,
        ]);
        $this->conn->prepare('DELETE FROM REPLAYFILE WHERE RUN_ID = :id')->execute([
            'id' => $runId,
        ]);
    }

    private function fetchRunFromResult($gmInfo)
    {
        $hash = $gmInfo['hash'];
        $reqsql = "SELECT * FROM RUN WHERE HASH=?";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $hash);
        $req->execute();
        return $req->fetch();
    }

    private function isReplayCheated($gmInfo)
    {
        $hash = $gmInfo['hash'];
        $reqsql = "SELECT INVALIDRUN_ID FROM INVALIDRUN WHERE HASH=?";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $hash);
        $req->execute();
        if ($req->rowCount() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function getGameMode($map, $runResult)
    {
        if ($map === null) {
            return false;
        }
        $stmt = $this->conn->prepare('SELECT * FROM GAMEMODE LEFT JOIN GAME ON GAMEMODE.GAME_ID = GAME.GAME_ID WHERE CODE_NAME = :codename AND DIFFICULTY_ID = :diff AND SPEED_ID = :speed');
        $stmt->bindValue('codename', $map['id']);
        $stmt->bindValue('diff', $runResult['game_diff']);
        $stmt->bindValue('speed', $runResult['game_speed'] - 1);
        $stmt->execute();
        return $stmt->fetch();
    }

    private function extractCV($json, $filename, $game_mode)
    {
        $run_json = $json["result"];
        if ($run_json["escaped"] == true) {
            $run_id = $this->insertRunCV($json, $filename, $game_mode);
            if ($run_id > 0) {
                $players_handles = $this->insertPlayersCV($json, $run_id);
                $insert_chalenges = $this->insertChallengesCV($json, $run_id, $players_handles, $game_mode);
                $table["error"] = "-1";
                $table["run_id"] = $run_id;
                return $table;
            } else {
                $table["error"] = "2";
                return $table;
            }
        } else {
            $table["error"] = "2";
            return $table;
        }
    }

    public function insertRunCV($json, $filename, $game_mode)
    {
        $run_json = $json["result"];
        $hash = $json['hash'];
        $client_version = explode(".", $json["general"]["client_version"]);
        $reqsql = "INSERT INTO RUN(TIME,GAMEMODE_ID,VERSION,ELAPSED_GAME_TIME,ELAPSED_REAL_TIME,ELAPSED_GAME_LOOPS,REPLAY_NAME,HASH,TIMESTAMP,CLIENT_VERSION,BUILD,REGION_ID,STARTED_AT_RT,STARTED_AT_GT) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        $req = $this->conn->prepare($reqsql);
        $req->bindParam(1, $run_json["escape_time"]);
        $req->bindParam(2, $game_mode);
        $req->bindParam(3, $run_json["game_version"]);
        $req->bindParam(4, $json["general"]["elapsed_game_time"]);
        $req->bindParam(5, $json["general"]["elapsed_real_time"]);
        $req->bindParam(6, $json["general"]["elapsed_game_loops"]);
        $req->bindParam(7, $filename);
        $req->bindParam(8, $hash);
        $req->bindParam(9, $json["general"]["timestamp"]);
        $req->bindValue(10, $client_version[0] . "." . $client_version[1] . "." . $client_version[2]);
        $req->bindValue(11, $client_version[3]);
        $req->bindParam(12, $json["general"]["server_region"]);
        $req->bindParam(13, $run_json["started_at_rt"]);
        $req->bindParam(14, $run_json["started_at_gt"]);
        $req->execute();
        if ($req->rowCount() > 0) {
            return $this->conn->lastInsertId();
        } else {
            return 0;
        }
    }

    public function insertPlayersCV($json, $run_id)
    {
        $players = $json["general"]["player_slots"];
        $players_handles = [];
        foreach ($players as $player) {
            $player_id = $player["player_id"];
            $player_run = $json["result"]["players"][$player_id];
            $color = $player["color"]["r"] . "," . $player["color"]["g"] . "," . $player["color"]["b"] . "," . $player["color"]["a"];
            $handle = $player["handle"];
            if ($handle === null) {
                $handle = $player["name"];
            }

            $reqsql = "SELECT CHARACTER_ID FROM `CHARACTER` WHERE HANDLE=?";
            $req = $this->conn->prepare($reqsql);
            $req->bindParam(1, $handle);
            $req->execute();
            if ($req->rowCount() > 0) {
                $data = $req->fetch();
                $handle_id = $data["CHARACTER_ID"];
            } else {
                $reqsql = "SELECT PLAYER_ID FROM `PLAYER` WHERE NAME=?";
                $req = $this->conn->prepare($reqsql);
                $req->bindParam(1, $player["name"]);
                $req->execute();
                if ($req->rowCount() > 0) {
                    $data = $req->fetch();
                    $player_id = $data["PLAYER_ID"];
                } else {
                    $reqsql = "INSERT INTO PLAYER(NAME,CLAN,TYPE) VALUES(?,?,?)";
                    $req = $this->conn->prepare($reqsql);
                    $req->bindParam(1, $player["name"]);
                    $req->bindParam(2, $player["clan"]);
                    $req->bindParam(3, $player["type"]);
                    $req->execute();
                    $player_id = $this->conn->lastInsertId();
                }

                $reqsql = "INSERT INTO `CHARACTER`(HANDLE,PLAYER_ID) VALUES(?,?)";
                $req = $this->conn->prepare($reqsql);
                $req->bindParam(1, $handle);
                $req->bindParam(2, $player_id);
                $req->execute();
                $handle_id = $this->conn->lastInsertId();
            }
            $reqsql = "INSERT INTO CHARACTER_RUN(CHARACTER_ID,RUN_ID,`LEFT`,LEVEL,REVIVE,DEATH,STIM,CREEP,RING,ART,TIMESHIFT_CREATE,TIMESHIFT_USE,RING_REVIVE,ART_REVIVE,APM,COLOR) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

            $req = $this->conn->prepare($reqsql);
            $req->bindParam(1, $handle_id);
            $req->bindParam(2, $run_id);
            $req->bindValue(3, (int) $player_run["left"]);
            $req->bindParam(4, $player_run["level"]);
            $req->bindParam(5, $player_run["revives"]);
            $req->bindParam(6, $player_run["deaths"]);
            $req->bindParam(7, $player_run["abilities_used"]["BOOST"]);
            $req->bindParam(8, $player_run["abilities_used"]["CREEP"]);
            $req->bindParam(9, $player_run["abilities_used"]["THROW_ESSENCE"]);
            $req->bindParam(10, $player_run["abilities_used"]["ART"]);
            $req->bindParam(11, $player_run["abilities_used"]["SHADE_CREATE"]);
            $req->bindParam(12, $player_run["abilities_used"]["SHADE_USE"]);
            $req->bindParam(13, $player_run["abilities_used"]["THROW_ESSENCE_REVIVE"]);
            $req->bindParam(14, $player_run["abilities_used"]["ART_REVIVE"]);
            $req->bindParam(15, $player["apm"]);
            $req->bindParam(16, $color);
            $req->execute();
            $players_handles[$handle] = $handle_id;
        }
        return $players_handles;
    }

    public function insertChallengesCV($json, $run_id, $players_handles, $game_mode)
    {
        $challenges = $json["result"]["challenges"];
        $old_offset = 0;
        $old_time = 0;
        foreach ($challenges as $key => $challenge) {
            $player_id_completed_by = $challenge["completed_by"][0][0];
            $handle_completed_by = "";
            $handle_completed_by = self::getHandleByPlayerId($json, $player_id_completed_by);

            $player_id_controlled_by = $challenge["completed_by"][0][1];
            $handle_controlled_by = "";
            $handle_controlled_by = self::getHandleByPlayerId($json, $player_id_controlled_by);

            $reqsql = "SELECT MAP_ID FROM MAP WHERE GAME_ID=(SELECT GAME_ID FROM GAMEMODE WHERE GAMEMODE_ID=?) AND MAP_NUMBER=?";
            $req = $this->conn->prepare($reqsql);
            $req->bindParam(1, $game_mode);
            $req->bindParam(2, $key);
            $req->execute();
            $data = $req->fetch();
            $map_id = $data["MAP_ID"];
            if ($player_id_completed_by == 15) {
                $handle_id_completed_by = -1;
            } else {
                $handle_id_completed_by = $players_handles[$handle_completed_by];
            }

            $challenge["time_offset_start"] = $challenge["time_offset_start"] ?? 0;

            if ($player_id_controlled_by == 15) {
                $handle_id_controlled_by = -1;
                $offset = $challenge["time_offset_start"] - $old_offset - $old_time;
                $old_offset = $challenge["time_offset_start"];
                $old_time = $challenge["completed_time"];
                $reqsql = "INSERT INTO RUN_MAP(MAP_ID,RUN_ID,TIME,`ORDER`,STARTED_AT,LOADING_TIME) VALUES(?,?,?,?,?,?)";
                $req = $this->conn->prepare($reqsql);
                $req->bindParam(1, $map_id);
                $req->bindParam(2, $run_id);
                $req->bindParam(3, $challenge["completed_time"]);
                $req->bindParam(4, $challenge["order"]);
                $req->bindParam(5, $challenge["time_offset_start"]);
                $req->bindParam(6, $offset);
                $req->execute();
                $run_map_id = $this->conn->lastInsertId();
                foreach ($players_handles as $player_handles) {

                    $reqsql = "INSERT INTO RUN_MAP_COMPLETE(COMPLETED_BY,CONTROLLED_BY,RUN_MAP_ID) VALUES(?,?,?)";
                    $req = $this->conn->prepare($reqsql);
                    $req->bindParam(1, $player_handles);
                    $req->bindParam(2, $player_handles);
                    $req->bindParam(3, $run_map_id);
                    $req->execute();
                }
            } else {

                $handle_id_controlled_by = $players_handles[$handle_controlled_by];

                $offset = $challenge["time_offset_start"] - $old_offset - $old_time;
                $old_offset = $challenge["time_offset_start"];
                $old_time = $challenge["completed_time"];
                $reqsql = "INSERT INTO RUN_MAP(MAP_ID,RUN_ID,TIME,`ORDER`,STARTED_AT,LOADING_TIME) VALUES(?,?,?,?,?,?)";
                $req = $this->conn->prepare($reqsql);
                $req->bindParam(1, $map_id);
                $req->bindParam(2, $run_id);
                $req->bindParam(3, $challenge["completed_time"]);
                $req->bindParam(4, $challenge["order"]);
                $req->bindParam(5, $challenge["time_offset_start"]);
                $req->bindParam(6, $offset);
                $req->execute();
                $run_map_id = $this->conn->lastInsertId();
                foreach ($challenge["completed_by"] as $player_finisher) {
                    $handle_completed_by = $this->getHandleByPlayerId($json, $player_finisher[0]);

                    $player_id_controlled_by = $player_finisher[1];
                    $handle_controlled_by = "";
                    $handle_controlled_by = $this->getHandleByPlayerId($json, $player_finisher[1]);

                    $reqsql = "INSERT INTO RUN_MAP_COMPLETE(COMPLETED_BY,CONTROLLED_BY,RUN_MAP_ID) VALUES(?,?,?)";
                    $req = $this->conn->prepare($reqsql);
                    $req->bindParam(1, $players_handles[$handle_completed_by]);
                    $req->bindParam(2, $players_handles[$handle_controlled_by]);
                    $req->bindParam(3, $run_map_id);
                    $req->execute();
                }
            }

            $powerups = $challenge["powerups_by"];
            $order = 1;
            foreach ($powerups as $powerup) {
                $handle_completed_by = $this->getHandleByPlayerId($json, $powerup[0]);
                $handle_controlled_by = $this->getHandleByPlayerId($json, $powerup[1]);
                $reqsql = "INSERT INTO RUN_MAP_POWERUP(COMPLETED_BY,CONTROLLED_BY,RUN_MAP_ID,`ORDER`) VALUES(?,?,?,?)";
                $req = $this->conn->prepare($reqsql);
                $req->bindParam(1, $players_handles[$handle_completed_by]);
                $req->bindParam(2, $players_handles[$handle_controlled_by]);
                $req->bindParam(3, $run_map_id);
                $req->bindParam(4, $order);
                $req->execute();
                $order++;
            }

            $buttons = $challenge["buttons_by"];
            $order = 1;
            foreach ($buttons as $button) {
                $handle_completed_by = $this->getHandleByPlayerId($json, $button[0]);
                $handle_controlled_by = $this->getHandleByPlayerId($json, $button[1]);
                $reqsql = "INSERT INTO RUN_MAP_BUTTON(COMPLETED_BY,CONTROLLED_BY,RUN_MAP_ID,`ORDER`) VALUES(?,?,?,?)";
                $req = $this->conn->prepare($reqsql);
                $req->bindParam(1, $players_handles[$handle_completed_by]);
                $req->bindParam(2, $players_handles[$handle_controlled_by]);
                $req->bindParam(3, $run_map_id);
                $req->bindParam(4, $order);
                $req->execute();
                $order++;
            }
        }
    }

    private function getHandleByPlayerId($json, $player_id)
    {
        foreach ($json["general"]["player_slots"] as $player) {
            if ($player["player_id"] === $player_id) {
                $handle = $player["handle"];
                if ($handle === null) {
                    $handle = $player["name"];
                }

                return $handle;
            }
        }
    }
}
