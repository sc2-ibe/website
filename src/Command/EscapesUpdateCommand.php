<?php

namespace App\Command;

use App\Controller\ExtractReplay;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;

class EscapesUpdateCommand extends Command
{
    protected static $defaultName = 'ibe:escapes:update';

    /**
     * @var Connection
     */
    protected $conn;

    /**
     * @var ExtractReplay
     */
    protected $extractReplay;

    public function __construct(Connection $conn, ExtractReplay $extractReplay)
    {
        parent::__construct();
        $this->conn = $conn;
        $this->extractReplay = $extractReplay;
    }

    protected function configure()
    {
        $this
            ->setDescription('Update stored escapes')
            ->addArgument('escape-id', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'Escape ID')
            ->addOption('offset', null, InputOption::VALUE_OPTIONAL, '', 0)
            ->addOption('game-code', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Game code')
            ->addOption('mode-id', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Game mode ID')
            ->addOption('update-filename', null, InputOption::VALUE_NONE, 'Update replay filename')
            ->addOption('update-basis', null, InputOption::VALUE_NONE, 'Update base information')
            ->addOption('update-details', null, InputOption::VALUE_NONE, 'Update details - challenges, powerups, buttons')
            ->addOption('trigger-webhook', null, InputOption::VALUE_NONE, 'Trigger webhooks - to post on Discord')
        ;
    }

    protected function rebuildEscapeDetails($run, $gmInfo)
    {
        $players_handles = $this->extractReplay->insertPlayersCV($gmInfo, $run['RUN_ID']);
        $this->extractReplay->insertChallengesCV($gmInfo, $run['RUN_ID'], $players_handles, $run['GAMEMODE_ID']);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $fs = new Filesystem();

        $runsList = $this->conn->query(
            'SELECT * FROM `RUN`
            LEFT JOIN `GAMEMODE` ON `RUN`.`GAMEMODE_ID` = `GAMEMODE`.`GAMEMODE_ID`
            LEFT JOIN `GAME` ON `GAMEMODE`.`GAME_ID` = `GAME`.`GAME_ID`
            ORDER BY `RUN_ID` ASC;'
        )->fetchAll();
        foreach ($runsList as $key => $run) {
            if (count($input->getArgument('escape-id')) > 0 && !in_array((string) $run['RUN_ID'], $input->getArgument('escape-id'))) {
                continue;
            }
            if (count($input->getOption('mode-id')) && array_search($run['GAMEMODE_ID'], $input->getOption('mode-id')) === false) {
                continue;
            }
            if (count($input->getOption('game-code')) && array_search($run['CODE_NAME'], $input->getOption('game-code')) === false) {
                continue;
            }
            if (intval($input->getOption('offset')) > ($key + 1)) {
                continue;
            }

            $run_id = $run['RUN_ID'];
            $io->writeln(sprintf("[%04d/%04d] %d - %s", $key + 1, count($runsList), $run['RUN_ID'], $run['REPLAY_NAME']));
            $submissionTimestamp = strtotime($run['SUBMITED_ON']);

            $repName = $run['REPLAY_NAME'];
            $absName = $this->extractReplay->getReplayAbsolutePath($repName);
            $gmInfo = $this->extractReplay->extract($absName);

            if (((time() - $gmInfo['general']['timestamp']) / (3600 * 24)) > 30) {
                $submissionTimestamp = $gmInfo['general']['timestamp'] + 3600;
            }

            if ($input->getOption('update-filename')) {
                $newName = $this->extractReplay->generateFilename($gmInfo);
                if ($newName !== $repName) {
                    $fs->rename($absName, $this->extractReplay->getReplayAbsolutePath($newName));
                    $title = $gmInfo["map"]["id"];
                    $this->conn->prepare('UPDATE `RUN` SET  REPLAY_NAME=? WHERE RUN_ID = ?')->execute([
                        $newName,
                        $run['RUN_ID'],
                    ]);
                }
            }

            $gameMode = $this->extractReplay->getGameMode($gmInfo['map'], $gmInfo['result']);
            if ($input->getOption('update-basis')) {
                $this->conn->prepare(
                    'UPDATE `RUN` SET
                    `HASH` = ?,
                    `ELAPSED_GAME_TIME` = ?,
                    `TIMESTAMP` = ?,
                    `TIME` = ?,
                    `VERSION` = ?,
                    `GAMEMODE_ID` = ?
                    WHERE RUN_ID = ?'
                )->execute([
                    $gmInfo['hash'],
                    $gmInfo['general']['elapsed_game_time'],
                    $gmInfo['general']['timestamp'],
                    $gmInfo['result']['escape_time'],
                    $gmInfo['result']['game_version'],
                    $gameMode['GAMEMODE_ID'],
                    $run['RUN_ID'],
                ]);

                if (isset($gmInfo['result']['started_at_rt']) || isset($gmInfo['result']['started_at_gt'])) {
                    $this->conn->prepare('UPDATE `RUN` SET STARTED_AT_RT = ?, STARTED_AT_GT = ? WHERE RUN_ID = ?')->execute([
                        $gmInfo['result']['started_at_rt'] ?? null,
                        $gmInfo['result']['started_at_gt'] ?? null,
                        $run['RUN_ID'],
                    ]);
                }

                $req = "SELECT * FROM RUN_MAP as rm JOIN MAP as m ON rm.MAP_ID=m.MAP_ID WHERE RUN_ID = $run_id ORDER BY `ORDER` ASC;";
                foreach ($this->conn->query($req)->fetchAll() as $challenge) {
                    $this->conn->prepare('UPDATE `RUN_MAP` SET  STARTED_AT = ? WHERE RUN_ID = ? AND `ORDER`=?')->execute([
                        $gmInfo['result']["challenges"][$challenge["MAP_NUMBER"]]["time_offset_start"] ?? null,
                        $run['RUN_ID'],
                        $challenge["ORDER"],
                    ]);
                }
            }

            if ($input->getOption('update-details')) {
                $this->extractReplay->removeRunDetails($run['RUN_ID']);
                $this->rebuildEscapeDetails($run, $gmInfo);
            }

            if ($input->getOption('trigger-webhook')) {
                $this->extractReplay->triggerWebhook($run['RUN_ID']);
            }
        }

        $io->success('Completed');
    }
}
