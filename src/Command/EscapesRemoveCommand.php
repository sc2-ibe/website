<?php

namespace App\Command;

use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Filesystem;
use App\Controller\ExtractReplay;

class EscapesRemoveCommand extends Command
{
    protected static $defaultName = 'ibe:escapes:remove';

    /**
     * @var Connection
     */
    protected $conn;

    /**
     * @var ExtractReplay
     */
    protected $extractReplay;

    /**
     * @var Filesystem
     */
    protected $fs;

    public function __construct(Connection $conn, ExtractReplay $extractReplay)
    {
        parent::__construct();
        $this->conn = $conn;
        $this->extractReplay = $extractReplay;
        $this->fs = new Filesystem();
    }

    protected function configure()
    {
        $this
            ->setDescription('Remove escape')
            ->addArgument('escape-id', InputArgument::REQUIRED, 'Escape ID')
            ->addOption('remove-file', null, InputOption::VALUE_NONE, 'Remove associated SC2Replay file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $stmt = $this->conn->prepare('SELECT * FROM `RUN` WHERE `RUN_ID` = :id');
        $stmt->bindValue('id', $input->getArgument('escape-id'));
        $stmt->execute();
        $run = $stmt->fetch();
        if ($run === false) {
            $io->error('Invalid ID');
            return 1;
        }

        $io->writeln(sprintf("%d - %s", $run['RUN_ID'], $run['REPLAY_NAME']));
        $this->extractReplay->removeRun($run['RUN_ID']);

        if ($input->getOption('remove-file')) {
            $this->fs->remove($this->extractReplay->getReplayAbsolutePath($run['REPLAY_NAME']));
        }
    }
}