<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Controller\ExtractReplay;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Filesystem\Filesystem;

class ReplaysRevaluateCommand extends Command
{
    protected static $defaultName = 'ibe:replays:revaluate';

    /**
     * @var Connection
     */
    protected $conn;

    /**
     * @var ExtractReplay
     */
    protected $extractReplay;

    /**
     * @var Filesystem
     */
    protected $fs;

    /**
     * @var boolean
    */
    protected $dryRun = false;

    public function __construct(Connection $conn, ExtractReplay $extractReplay)
    {
        $this->conn = $conn;
        $this->extractReplay = $extractReplay;
        $this->fs = new Filesystem();
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Revaluate submitted replays')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Don\'t perform any changes')
            ->addOption('file-hash', null, InputOption::VALUE_OPTIONAL | InputOption::VALUE_IS_ARRAY, 'Replay file hash')
            ->addOption('offset', null, InputOption::VALUE_OPTIONAL, '', 0)
            ->addOption('stop-on-error', null, InputOption::VALUE_NONE, 'Stop on error')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $this->dryRun = $input->getOption('dry-run');

        $repList = $this->conn->query('SELECT * FROM REPLAYFILE WHERE RESULT_CODE = 1 ORDER BY `CREATED_AT` ASC;')->fetchAll();
        foreach ($repList as $key => $replay) {
            if (count($input->getOption('file-hash')) && array_search($replay['FILE_HASH'], $input->getOption('file-hash')) === false) {
                continue;
            }
            if (intval($input->getOption('offset')) > ($key + 1)) {
                continue;
            }

            $io->writeln(sprintf('[%04d/%04d] (%s) %s', $key + 1, count($repList), $replay['FILE_HASH'], $replay['FILE_NAME']));
            
            $absName = $this->extractReplay->getErrReplayAbsolutePath($replay['FILE_HASH'] . '.SC2Replay');
            if (!$this->fs->exists($absName)) {
                $io->error('File doesn\'t exist');
                $this->deleteReplayFile($replay);
                continue;
            }

            try {
                $gmInfo = $this->extractReplay->extract($absName);
            } catch (ProcessFailedException $e) {
                $process = $e->getProcess();
                /** @var Process $process */

                if ($process->getExitCode() === 2) {
                    $this->deleteReplayFile($replay);
                    $io->writeln('Not supported');
                    continue;
                }

                if ($input->getOption('stop-on-error')) {
                    if (strstr($process->getErrorOutput(), 'Old BTB not yet supported') !== false) {
                        continue;
                    }
                    $io->write($process->getErrorOutput());
                    $io->error($process->getExitCode());
                    break;
                } else {
                    $io->error($process->getExitCode());
                    continue;
                }
            }

            if ($gmInfo['result'] === null) {
                $io->writeln('Not escaped');
                $this->deleteReplayFile($replay);
                continue;
            }

            $io->writeln($this->extractReplay->generateFilename($gmInfo, false));

            if ($this->dryRun) {
                continue;
            }

            $res = $this->extractReplay->submitReplay($gmInfo, $absName, $replay['FILE_NAME']);
            if ($res['success']) {
                $io->success($res['message']);
            } else {
                $io->error($res['message']);
                $this->deleteReplayFile($replay);
            }
        }
    }

    protected function deleteReplayFile($replay) {
        if ($this->dryRun) {
            return;
        }
        
        $this->extractReplay->deleteReplayByFileHash($replay['FILE_HASH']);
    }
}
