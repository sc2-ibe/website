<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\DBAL\Driver\Connection;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Psr\Log\LoggerInterface;

class BaseController extends AbstractController
{
    /**
     * @var Connection
    */
    protected $conn;

    /**
     * @var AdapterInterface
    */
    protected $cache;

    /**
     * @var LoggerInterface
    */
    protected $logger;


    /**
     * @required
     */
    public function setConn(Connection $conn)
    {
        $this->conn = $conn;

        return $this;
    }

    /**
     * @required
     */
    public function setCache(AdapterInterface $cache)
    {
        $this->cache = $cache;

        return $this;
    }

    /**
     * @required
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;

        return $this;
    }
}