<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Carbon\CarbonInterval;
use Carbon\Carbon;

class AppExtension extends AbstractExtension
{
    public function getFilters(): array
    {
        return [
            new TwigFilter('time_elapsed', [$this, 'formatElapsed'], [
                'is_safe' => ['html'],
            ]),
            new TwigFilter('carbon_diff', [$this, 'carbonDiff'], [
                'is_safe' => ['html'],
            ]),
        ];
    }

    public function getFunctions(): array
    {
        return [
            // new TwigFunction('function_name', [$this, 'formatElapsed']),
        ];
    }

    public function formatElapsed($value, $includeFractions = false)
    {
        if ($value === null) {
            return 'N/A';
        }
        $r = CarbonInterval::seconds(floor($value))->cascade()->forHumans(['join' => ' '], true, 3);
        $r = preg_replace('/ ([0-9])([ms])/', ' 0$1$2', $r);
        if ($includeFractions) {
            $fractions = floor(((float) $value - floor($value)) * 100.0);
            $r = preg_replace('/([0-9]+)(s)/', '$1$2 <small>[.' . str_pad($fractions, 2, '0', STR_PAD_LEFT) . ']</small>', $r);
        }
        return $r;
    }

    public function carbonDiff($timestamp)
    {
        return Carbon::createFromTimestamp($timestamp)->diffForHumans(Carbon::now(), \Carbon\CarbonInterface::DIFF_RELATIVE_TO_NOW);
    }
}
