const Encore = require('@symfony/webpack-encore');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

Encore
    // directory where compiled assets will be stored
    .setOutputPath('public/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/build')
    // only needed for CDN's or sub-directory deploy
    //.setManifestKeyPrefix('build/')

    .copyFiles([
        {
            from: './assets/images',

            // optional target path, relative to the output dir
            //to: 'images/[path][name].[ext]',

            // if versioning is enabled, add the file hash too
            to: 'images/[path][name].[hash:8].[ext]',

            // only copy files matching this pattern
            // pattern: /\.(png|jpg|jpeg|ico|xml|json)$/
        },
        {
            from: './assets/favicon',
            to: 'favicon/[name].[ext]',
        }
    ])

    /*
     * ENTRY CONFIG
     *
     * Add 1 entry for each "page" of your app
     * (including one that's included on every page - e.g. "app")
     *
     * Each entry will result in one JavaScript file (e.g. app.js)
     * and one CSS file (e.g. app.css) if you JavaScript imports CSS.
     */
    .addEntry('app', './assets/js/app.js')
    .addStyleEntry('vendor', './assets/scss/vendor.scss')
    .addStyleEntry('common', './assets/scss/common.scss')
    .addStyleEntry('importReplay', './assets/css/importReplay.css')

    // will require an extra script tag for runtime.js
    // but, you probably want this, unless you're building a single-page app
    .enableSingleRuntimeChunk()

    /*
     * FEATURE CONFIG
     *
     * Enable & configure other features below. For a full
     * list of features, see:
     * https://symfony.com/doc/current/frontend.html#adding-more-features
     */
    .cleanupOutputBeforeBuild()
    // .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables Sass/SCSS support
    .enableSassLoader()

    .configureBabel(function (babelConfig) {
        // add additional presets
        // babelConfig.presets.push('es2015');

        // no plugins are added by default, but you can add some
        // babelConfig.plugins.push('styled-jsx/babel');
    })

    // uncomment if you use TypeScript
    //.enableTypeScriptLoader()

    // uncomment if you're having problems with a jQuery plugin
    //.autoProvidejQuery()

    // uncomment if you use API Platform Admin (composer req api-admin)
    //.enableReactPreset()
    //.addEntry('admin', './assets/js/admin.js')
    ;

const config = Encore.getWebpackConfig();

config.watchOptions = { poll: true, ignored: /node_modules/ };

config.plugins.push(
    new BrowserSyncPlugin(
        {
            proxy: process.env.SYMFONY_PROXY_URL || '127.0.0.1:8080',
            open: false,
            files: [
                {
                    match: [
                        'public/build/**/*.css',
                    ],
                    fn: function (event, file) {
                        if (event === 'change') {
                            const bs = require('browser-sync').get('bs-webpack-plugin');
                            bs.reload(file);
                        }
                    }
                },
                {
                    match: [
                        'public/build/**/*.js',
                        'templates/**/*.twig',
                        'src/Controller/**/*.php',
                    ],
                    fn: function (event, file) {
                        if (event === 'change') {
                            const bs = require('browser-sync').get('bs-webpack-plugin');
                            bs.reload();
                        }
                    }
                }
            ]
        },
        {
            reload: false, // this allow webpack server to take care of instead browser sync
            name: 'bs-webpack-plugin',
        }
    )
);

config.module.rules.unshift({
    parser: {
        amd: false,
    }
});

module.exports = config;
