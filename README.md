# IBE Rankings website

https://rankings.icebanelingescape.com/

### Development workflow

**Initial setup**

```
git clone ...
git submodule update
composer install
yarn install
```

### Frontend worfklow

`yarn run dev-watch`

Runs the build proces with `dev` preset and listens for further changes. Additionally it will launch `browsersync` proxy, which if used will reload browser upon file modification.

`yarn run build`

Runs the build with `production` preset. Everything will be compressed and taken care of appropriately.

### Deployment

....

`bin/dep deploy`
