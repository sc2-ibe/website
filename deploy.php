<?php

namespace Deployer;

require 'recipe/symfony4.php';

// Project name
set('application', 'ibe-rank');
set('allow_anonymous_stats', false);

// Project repository
set('repository', 'https://gitlab.com/sc2-ibe/website.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

// Shared files/dirs between deploys
add('shared_files', []);
add('shared_dirs', [
    'public/uploads',
    'var/backups',
]);

// Writable dirs by web server
add('writable_dirs', []);
set('writable_mode', 'chown');
set('http_user', 'ibe');
set('http_group', 'www-data');
set('bin/composer', '~/.local/bin/composer');
//
set('bin/php', function () {
    return 'APP_ENV=prod ' . run('which php7.3') . ' -d memory_limit=2G';
});

// Hosts

host('ibe.talv.space')
    ->set('deploy_path', '~/{{application}}')
    ->user('ibe')
    ->identityFile('~/.ssh/id_rsa');

// Tasks

task('webpack:build', function () {
    runLocally('yarn run build');
});

task('webpack:deploy', function () {
    upload('public/build/', '{{release_path}}/public/build');
});

task('httpd:reload', function () {
    run('sudo httpd-reload');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.
// before('deploy:symlink', 'database:migrate');

// reload apache2 after successful deploy
after('deploy:symlink', 'httpd:reload');

// recompile & upload frontend assets before proceeding
after('deploy:shared', 'webpack:build');
after('deploy:shared', 'webpack:deploy');

//
// MySQL sync
//

function mysqlDumpCommand($config, $filename)
{
    return 'mysqldump' .
        ' --user=' . escapeshellarg($config['user']) .
        ' --password=' . escapeshellarg($config['pass']) .
        ' --host=' . escapeshellarg($config['host']) .
        ' ' . escapeshellarg($config['database']) .
        ' | sed -e ' . escapeshellarg('s/DEFINER[ ]*=[ ]*[^*]*\*/\*/') .
        ' | gzip -c > ' . ($filename);
}

function mysqlRestoreCommand($config, $filename)
{
    return 'gzip -d -c ' . ($filename) .  ' | mysql' .
        ' --user=' . escapeshellarg($config['user']) .
        ' --password=' . escapeshellarg($config['pass']) .
        ' --host=' . escapeshellarg($config['host']) .
        ' ' . escapeshellarg($config['database']);
}

function mysqlFetchEnvCredentials($data)
{
    $r = preg_match('/^DATABASE_URL=mysql:\/\/(?<user>[\w\.]+):(?<pass>[\w\.]+)@(?<host>[\w\.]+):(?<port>[0-9]+)\/(?<database>\w+)$/', $data, $matches);
    if (!$r) throw new \Exception();
    return $matches;
}
function mysqlFetchEnvLobbiesCredentials($data)
{
    $r = preg_match('/^DATABASE_LOBBIES_URL=mysql:\/\/(?<user>[\w\.]+):(?<pass>[\w\.]+)@(?<host>[\w\.]+):(?<port>[0-9]+)\/(?<database>\w+)$/', $data, $matches);
    if (!$r) throw new \Exception();
    return $matches;
}

function mysqlConfigLocal()
{
    return mysqlFetchEnvCredentials(runLocally('grep -P "^DATABASE_URL=" .env'));
}

function mysqlConfigRemote()
{
    return mysqlFetchEnvCredentials(run('grep -P "^DATABASE_URL=" {{current_path}}/.env'));
}


function mysqlConfigLobbiesLocal()
{
    return mysqlFetchEnvLobbiesCredentials(runLocally('grep -P "^DATABASE_LOBBIES_URL=" .env'));
}
function mysqlConfigLobbiesRemote()
{
    return mysqlFetchEnvLobbiesCredentials(run('grep -P "^DATABASE_LOBBIES_URL=" {{current_path}}/.env'));
}

task('mysql:push', function () {
    $cfgLocal = mysqlConfigLocal();
    $cfgRemote = mysqlConfigRemote();

    // do backup
    run('mkdir -p {{current_path}}/var/backups');
    run(mysqlDumpCommand($cfgRemote, '{{current_path}}/var/backups/' . $cfgRemote['database'] . '_' . date('Y-m-d_Hi') . '.sql.gz'));

    // push to remote
    $tmpEnvFile = tmpfile();
    runLocally(mysqlDumpCommand($cfgLocal, stream_get_meta_data($tmpEnvFile)['uri']));
    upload(stream_get_meta_data($tmpEnvFile)['uri'], '{{deploy_path}}/.tmpdump.sql.gz');
    fclose($tmpEnvFile);
    run(mysqlRestoreCommand($cfgRemote, '{{deploy_path}}/.tmpdump.sql.gz'));
    run('rm {{deploy_path}}/.tmpdump.sql.gz');
});

task('mysql:pull', function () {
    $cfgLocal = mysqlConfigLocal();
    $cfgRemote = mysqlConfigRemote();

    // do backup
    runLocally('mkdir -p var/backups');
    runLocally(mysqlDumpCommand($cfgLocal, 'var/backups/' . $cfgLocal['database'] . '_' . date('Y-m-d_Hi') . '.sql.gz'));

    // pull remote
    run(mysqlDumpCommand($cfgRemote, '{{deploy_path}}/.tmpdump.sql.gz'));
    $tmpEnvFile = tmpfile();
    download('{{deploy_path}}/.tmpdump.sql.gz', stream_get_meta_data($tmpEnvFile)['uri']);
    run('rm {{deploy_path}}/.tmpdump.sql.gz');
    runLocally(mysqlRestoreCommand($cfgLocal, stream_get_meta_data($tmpEnvFile)['uri']));
    fclose($tmpEnvFile);
});

task('mysql:lobbies-pull', function () {
    $cfgLocal = mysqlConfigLobbiesLocal();
    $cfgRemote = mysqlConfigLobbiesRemote();

    // do backup
    //runLocally('mkdir -p var/backups');
    //runLocally(mysqlDumpCommand($cfgLocal, 'var/backups/' . $cfgLocal['database'] . '_' . date('Y-m-d_Hi') . '.sql.gz'));

    // pull remote
    run(mysqlDumpCommand($cfgRemote, '{{deploy_path}}/.tmpdump.sql.gz'));
    $tmpEnvFile = tmpfile();
    download('{{deploy_path}}/.tmpdump.sql.gz', stream_get_meta_data($tmpEnvFile)['uri']);
    //run('rm {{deploy_path}}/.tmpdump.sql.gz');
    //runLocally(mysqlRestoreCommand($cfgLocal, stream_get_meta_data($tmpEnvFile)['uri']));
    fclose($tmpEnvFile);
});
