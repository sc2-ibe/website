# syntax = docker/dockerfile:1.4.2-labs

FROM node:10-alpine as build-dist

ENV YARN_CACHE_FOLDER=/tmp/.yarn_cache

WORKDIR /app

COPY package.json yarn.lock ./

RUN --mount=type=cache,target=/tmp/.yarn_cache <<EOF
    set -eux
    yarn install --pure-lockfile --no-interactive
EOF

COPY webpack.config.js ./
COPY assets/ assets/
RUN mkdir -p public

RUN <<EOF
    set -eux
    yarn run build
EOF


FROM webdevops/php:7.1-alpine as build-app

ARG APP_ENV

ENV APP_ENV=${APP_ENV}

ENV COMPOSER_CACHE_DIR=/tmp/.cache_composer

WORKDIR /app

COPY . .

RUN --mount=type=cache,target=/tmp/.cache_composer --mount=type=ssh <<EOF
    # composer1 install --optimize-autoloader --no-dev
    composer1 install --optimize-autoloader
EOF


FROM webdevops/php-apache:7.1-alpine as prod

RUN <<EOF
    python -m ensurepip
EOF

WORKDIR /app

ENV WEB_DOCUMENT_ROOT=/app/public
ENV WEB_DOCUMENT_INDEX=index.php

RUN rmdir /app
COPY --from=build-app --chown=application:application /app /app

RUN <<EOF
    python -m pip install /app/sc2-repdump
    python -m pip install sc2reader
EOF

COPY --from=build-dist --chown=application:application /app/public/build /app/public/build
